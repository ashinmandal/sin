var SIN = SIN || {};

SIN.Game = (function (doc) {
  this.game = new Phaser.Game(1000, 600, Phaser.AUTO, 'content');
  this.game.state.add('Level1Boot', SIN.Level1Boot);
  this.game.state.add('ProloguePreload', SIN.ProloguePreload);
  this.game.state.add('Prologue', SIN.Prologue);
  this.game.state.add('Level1Preload', SIN.Level1Preload);
  this.game.state.add('Level1Game', SIN.Level1Game);
  this.game.state.add('Level2Preload', SIN.Level2Preload);
  this.game.state.add('Level2Game', SIN.Level2Game);
  this.game.state.add('Level3Preload', SIN.Level3Preload);
  this.game.state.add('Level3Game', SIN.Level3Game);
  this.game.state.add('Level4Preload', SIN.Level4Preload);
  this.game.state.add('Level4Game', SIN.Level4Game);
  this.game.state.start('Level1Boot');
})(document);
