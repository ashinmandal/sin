var SIN = SIN || {};

SIN.Level3Game = function(){
  this.healthbar = [];
  this.player = [];
  this.height = [];
  this.down = [];
  this.wall = [];
  this.timeCheck = [];
  this.isWallEnabled = [];
};

SIN.Level3Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level3Game.prototype.constructor = SIN.Level2Game;

SIN.Level3Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level3Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2002, 700, 'level3_bg_1');
  this.game.add.tileSprite(2002, 0, 2002, 700, 'level3_bg_2');
  this.game.add.tileSprite(4004, 0, 2002, 700, 'level3_bg_3');
  this.game.add.tileSprite(6006, 0, 2002, 700, 'level3_bg_4');
  this.game.add.tileSprite(8008, 0, 2002, 700, 'level3_bg_5');

  this.map = this.game.add.tilemap('level3');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Phase III Spritesheet', 'Phase III Spritesheet');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.metalGroundLayer = this.map.createLayer('metalGroundLayer');
  this.lavaPoolLayer = this.map.createLayer('lavaPoolLayer');
  this.barrelLayer = this.map.createLayer('barrelLayer');
  this.barricadeLayer = this.map.createLayer('barricadeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'metalGroundLayer');
  this.map.setCollisionBetween(1, 100000, true, 'lavaPoolLayer');
  this.map.setCollisionBetween(1, 100000, true, 'barrelLayer');
  this.map.setCollisionBetween(1, 100000, true, 'barricadeLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.wall = game.add.sprite(9000, 630, 'wall');
  this.game.physics.arcade.enable(this.wall);
  this.wall.cropEnabled = true;
  this.wall.anchor.setTo(0.5,1);
  this.height = 490;
  this.down = true;
  this.isWallEnabled = false;

  // Creating weapons for Level 3
  this.axeGroup = game.add.physicsGroup();

  this.axe1_1 = new SIN.Axe1(game, 1290, -400, 10, 850);
  this.axeGroup.add(this.axe1_1);

  this.axe1_2 = new SIN.Axe1(game, 2300, -400, 10, 850);
  this.axeGroup.add(this.axe1_2);

  this.axe1_3 = new SIN.Axe1(game, 3360, -400, -60, 900);
  this.axeGroup.add(this.axe1_3);

  this.axe2_1 = new SIN.Axe2(game, 4580, -400, 10, 950);
  this.axeGroup.add(this.axe2_1);

  this.axe2_2 = new SIN.Axe2(game, 5140, -400, 10, 950);
  this.axeGroup.add(this.axe2_2);

  this.axe2_3 = new SIN.Axe2(game, 5700, -400, 10, 950);
  this.axeGroup.add(this.axe2_3);

  this.axe3_1 = new SIN.Axe3(game, 6730, -400, 10, 850);
  this.axeGroup.add(this.axe3_1);

  this.axe3_1 = new SIN.Axe3(game, 7370, -400, 10, 850);
  this.axeGroup.add(this.axe3_1);

  this.axe3_3 = new SIN.Axe3(game, 8750, -400, 10, 850);
  this.axeGroup.add(this.axe3_3);

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;

  this.fireballGroup = game.add.physicsGroup();

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 100, 260);
  //this.player = new SIN.Degir(game, 10000, 160);

  this.collected2 = this.game.add.sprite(20, 20, 'collected2');
  this.collected2.fixedToCamera = true;
  this.collected3 = this.game.add.sprite(20, 20, 'collected3');
  this.collected3.fixedToCamera = true;
  this.collected3.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level3Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.metalGroundLayer);
  this.game.physics.arcade.collide(this.player, this.lavaPoolLayer, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.player, this.barrelLayer);
  this.game.physics.arcade.collide(this.player, this.barricadeLayer, this.playerHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.axeGroup, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.fireball, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.wall, this.playerWallHit, null, this);

  if (this.isWallEnabled) {
    if (this.down) {
      this.game.time.events.add(Phaser.Timer.SECOND * 0.8, this.decrease, this);
    } else {
      this.game.time.events.add(Phaser.Timer.SECOND * 0.8, this.increase, this);
    }
  }

  if (this.player.body.x > 2695 && this.player.body.x < 3535 && game.time.now - this.timeCheck > 3000) {
    this.fireball = this.fireballGroup.create((this.player.body.x + 900), 70, 'fireball');
    this.fireball.checkWorldBounds = true;
    this.fireball.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.fireball);
    this.fireball.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x > 6335 && this.player.body.x < 7640 && game.time.now - this.timeCheck > 4000) {
    this.fireball = this.fireballGroup.create((this.player.body.x + 900), 490, 'fireball');
    this.fireball.checkWorldBounds = true;
    this.fireball.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.fireball);
    this.fireball.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 1239 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level3_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 2924 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level3_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 10000) {
    this.bgMusic.destroy();
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected2.alpha = 1;
    this.collected3.alpha = 0;

    this.dialogue1Said = false;
    this.dialogue2Said = false;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.isWallEnabled = false;
      this.createKey();
    }
  }
};

SIN.Level3Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level3Game.prototype.resetLevel = function() {
  this.fireballGroup.forEach(function(fire) {
    fire.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level3Game.prototype.createKey = function() {
  this.key = game.add.sprite(2695, 31, 'key3');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 51}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level3Game.prototype.playerHit = function() {
  //if hits on the right side, die
  console.log("Overlapping but no blocked.right");
  if(this.player.body.blocked.right) {
    //set to dead (this doesn't affect rendering)
    this.player.health -= 1;
    this.player.body.x -= 30;
    cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
    console.log("Health bar width at playerHit() - collision with barricadeLayer: ", cropWidth);
    this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
    if (this.player.health <= 0) {
        this.resetLevel();
    }
  }
};

SIN.Level3Game.prototype.playerWallHit = function() {
  //if hits on the right side, die
  if(this.down && this.wall.height > 10) {
      this.resetLevel();
  }
};

SIN.Level3Game.prototype.decrease = function() {
  if (this.wall.height > 0) {
    this.wall.crop(new Phaser.Rectangle(0, 0, this.wall.width, this.height));
    this.height -= 10;
  }
  if (this.height < 0) {
    this.down = false;
  }
};

SIN.Level3Game.prototype.increase = function() {
  if (this.wall.height < 490) {
    this.wall.crop(new Phaser.Rectangle(0, 0, this.wall.width, this.height));
    this.height += 10;
  }
  if (this.height > 490) {
    this.down = true;
    this.height = 490;
  }
};

SIN.Level3Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected2.alpha = 0;
    this.collected3.alpha = 1;
    this.isWallEnabled = true;
  }, this);
};
