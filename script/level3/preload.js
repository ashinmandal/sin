var SIN = SIN || {};

SIN.Level3Preload = function(){};

SIN.Level3Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level3Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading3');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level3', 'assets/tilemaps/level3.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Phase III Spritesheet', 'assets/images/level3/spritesheet.png');

  this.load.image('level3_bg_1', 'assets/images/level3/bg_1.png');
  this.load.image('level3_bg_2', 'assets/images/level3/bg_2.png');
  this.load.image('level3_bg_3', 'assets/images/level3/bg_3.png');
  this.load.image('level3_bg_4', 'assets/images/level3/bg_4.png');
  this.load.image('level3_bg_5', 'assets/images/level3/bg_5.png');

  this.load.image('axe1', 'assets/images/weapons/axe1.png');
  this.load.image('axe2', 'assets/images/weapons/axe2.png');
  this.load.image('axe3', 'assets/images/weapons/axe3.png');

  this.load.image('key3', 'assets/images/level3/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.audio('level3_dialogue1', 'assets/audio/vo_degir_face.ogg');
  this.load.audio('level3_dialogue2', 'assets/audio/vo_degir_key3.ogg');
  this.load.image('collected2', 'assets/images/collected2.png');
  this.load.image('collected3', 'assets/images/collected3.png');
  this.load.image('wall', 'assets/images/weapons/wall.png');
  this.load.image('fireball', 'assets/images/weapons/fireball.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level3Preload.prototype.create = function() {
  this.game.state.start('Level3Game');
};
