var SIN = SIN || {};

SIN.Level1Boot = function(){};

SIN.Level1Boot.prototype = Object.create(Phaser.State.prototype);

SIN.Level1Boot.prototype.preload = function() {
  //assets we'll use in the loading screen
  this.load.image('healthbar', 'assets/images/healthbar.png');
  this.load.image('logo', 'assets/images/logo.png');
  this.load.image('loading1', 'assets/images/level1/loading1.png');
  this.load.image('loading2', 'assets/images/level2/loading2.png');
  this.load.image('loading3', 'assets/images/level3/loading3.png');
  this.load.image('loading4', 'assets/images/level4/loading4.png');
};

SIN.Level1Boot.prototype.create = function() {
  //loading screen will have a white background
  this.game.stage.backgroundColor = '#000';

  //scaling options
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.forceLandscape = true;
  this.scale.pageAlignHorizontally = true;
  //this.scale.updateLayout();

  //physics system
  this.game.physics.startSystem(Phaser.Physics.ARCADE);

  this.game.state.start('ProloguePreload');
};
