var SIN = SIN || {};

SIN.Level1Preload = function(){};

SIN.Level1Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level1Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading1');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level1', 'assets/tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Spritesheet_35px', 'assets/images/level1/spritesheet.png');
  this.load.image('Boat', 'assets/images/level1/Boat.png');
  this.load.image('level1_bg_1', 'assets/images/level1/bg_1.png');
  this.load.image('level1_bg_2', 'assets/images/level1/bg_2.png');
  this.load.image('level1_bg_3', 'assets/images/level1/bg_3.png');

  this.load.audio('music_calm', 'assets/audio/music_calm.ogg');
  this.load.audio('level1_dialogue1', 'assets/audio/vo_degir_key1.ogg');
  this.load.audio('level1_dialogue2', 'assets/audio/vo_degir_boat.ogg');

  this.load.image('key1', 'assets/images/level1/key.png');
  this.load.image('collected0', 'assets/images/collected0.png');
  this.load.image('collected1', 'assets/images/collected1.png');

  this.load.image('knife', 'assets/images/weapons/knife.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level1Preload.prototype.create = function() {
  this.game.state.start('Level1Game');
};
