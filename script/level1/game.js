var SIN = SIN || {};

SIN.Level1Game = function(){
  this.locked = [];
  this.lockedTo = [];
  this.boat = [];
  this.healthbar = [];
  this.player = [];
  this.boatStarted = [];
  this.timeCheck = [];
};

SIN.Level1Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level1Game.prototype.constructor = SIN.Level1Game;

SIN.Level1Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
  this.locked = false;
  this.lockedTo = null;
  this.isBoatAnchored = true;
};

SIN.Level1Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 1750, 700, 'level1_bg_1');
  this.game.add.tileSprite(1750, 0, 1750, 700, 'level1_bg_2');
  this.game.add.tileSprite(3500, 0, 1750, 700, 'level1_bg_3');
  this.map = this.game.add.tilemap('level1');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Spritesheet_35px', 'Spritesheet_35px');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.rockLayer = this.map.createLayer('rockLayer');
  this.waterLayer = this.map.createLayer('waterLayer');
  this.barricadeLayer = this.map.createLayer('barricadeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'barricadeLayer');
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'rockLayer');
  this.map.setCollisionBetween(1, 100000, true, 'waterLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;


  // creating custom sprite objects
  this.boat = new SIN.Boat(game, 3720, 525);
  this.player = new SIN.Degir(game, 100, 260);
  this.healthbar = new SIN.HealthBar(game, 95, 30);

  this.collected0 = this.game.add.sprite(20, 20, 'collected0');
  this.collected0.fixedToCamera = true;
  this.collected1 = this.game.add.sprite(20, 20, 'collected1');
  this.collected1.fixedToCamera = true;
  this.collected1.alpha = 0;

  this.knifeGroup = game.add.physicsGroup();
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);

  this.bgMusic = this.game.add.audio('music_calm');
  this.bgMusic.loopFull(0.5);
};

SIN.Level1Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.barricadeLayer, this.playerHit, null, this);
  this.game.physics.arcade.collide(this.player, this.rockLayer, this.playerRockHit, null, this);
  this.game.physics.arcade.collide(this.player, this.waterLayer, this.playerRockHit, null, this);
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.collide(this.player, this.boat, this.playerBoatHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.knife, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.boat, this.waterLayer);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);

  if (this.locked) {
    this.checkLock();
  }

  if (this.locked && (this.jumpKey.isDown || this.cursors.up.isDown)) {
    this.cancelLock();
    this.player.body.velocity.y -= 650;
  }

  if (this.player.body.x >= 3350 && this.isBoatAnchored === false) {
    this.isBoatAnchored = true;
    this.boat.body.velocity.x = 80;
  }

  if (this.player.body.x > 500 && this.player.body.x < 3000 && game.time.now - this.timeCheck > 5000) {
    this.knife = this.knifeGroup.create((this.player.body.x + 900), (350 + Math.random() * 80), 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 2300 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level1_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 2960 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level1_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 5200) {
    this.bgMusic.destroy();
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected0.alpha = 1;
    this.collected1.alpha = 0;

    this.dialogue1Said = false;
    this.dialogue2Said = false;

    this.boat.body.x = 3720;
    this.boat.body.y = 525;
    this.boat.body.velocity.x = 0;
    this.isBoatAnchored = true;

    if (this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
    }
  }
};

SIN.Level1Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level1Game.prototype.resetLevel = function() {
  this.knifeGroup.forEach(function(kni) {
    kni.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level1Game.prototype.createKey = function() {
  this.key = game.add.sprite(2430, 42, 'key1');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 62}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level1Game.prototype.playerHit = function(player, barricadeLayer) {
  //if hits on the right side, die
  if(player.body.blocked.right) {
    //set to dead (this doesn't affect rendering)
    this.player.health -= 1;
    this.player.body.x -= 30;
    cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
    console.log("Health bar width at playerHit() - collision with barricadeLayer: ", cropWidth);
    this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
    if (this.player.health <= 0) {
        this.resetLevel();
    }
  }
};

SIN.Level1Game.prototype.playerRockHit = function(player, rockLayer) {
  //if hits on the right side, die
  if((this.player.body.blocked.down || this.player.body.blocked.right) && this.locked === false) {
    this.resetLevel();
  }
};

SIN.Level1Game.prototype.checkLock = function() {
  this.player.body.velocity.y = 0;
  //  If the player has walked off either side of the platform then they're no longer locked to it
  if (this.player.body.right < this.lockedTo.body.x || this.player.body.x > this.lockedTo.body.right)
  {
      this.cancelLock();
  }
};

SIN.Level1Game.prototype.cancelLock = function() {
  this.locked = false;
};

SIN.Level1Game.prototype.preRender = function() {
  if (this.game.paused)
  {
      //  Because preRender still runs even if your game pauses!
      return;
  }

  if (this.locked)
  {
      this.player.x += this.lockedTo.deltaX;
      this.player.y = this.lockedTo.y + 100;

      if (this.player.body.velocity.x !== 0)
      {
          this.player.body.velocity.y = 0;
      }
  }
};

SIN.Level1Game.prototype.playerBoatHit = function(player, boat) {
  if (!this.locked && player.body.velocity.y > 0)
  {
      this.locked = true;
      this.lockedTo = boat;
      boat.playerLocked = true;

      player.body.velocity.y = 0;
  }
};

SIN.Level1Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected0.alpha = 0;
    this.collected1.alpha = 1;
    this.isBoatAnchored = false;
  }, this);
};
