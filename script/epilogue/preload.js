var SIN = SIN || {};

SIN.EpiloguePreload = function(){};

SIN.EpiloguePreload.prototype = Object.create(Phaser.State.prototype);

SIN.EpiloguePreload.prototype.preload = function() {
  //show loading screen
  //this.logo = this.game.add.sprite(this.game.world.centerX, (this.game.world.centerY - 20), 'logo');
  //this.logo.anchor.setTo(0.5);

  this.preloadBar = this.add.sprite((this.game.world.centerX - 60), (this.game.world.centerY + 100), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);
  this.load.image('epilogue1', 'assets/images/epilogue/1.png');
  this.load.image('epilogue2', 'assets/images/epilogue/2.png');
  this.load.image('epilogue3', 'assets/images/epilogue/3.png');
  this.load.image('epilogue4', 'assets/images/epilogue/4.png');
  this.load.image('epilogue5', 'assets/images/epilogue/5.png');
  this.load.image('epilogue6', 'assets/images/epilogue/6.png');
  this.load.image('epilogue7', 'assets/images/epilogue/7.png');
  this.load.image('epilogue8', 'assets/images/epilogue/8.png');
  this.load.image('epilogue9', 'assets/images/epilogue/9.png');
  this.load.image('epilogue10', 'assets/images/epilogue/10.png');
  this.load.audio('epilogue_dialogue', 'assets/audio/vo_sin_epilogue.ogg');
  this.load.audio('music_calm', 'assets/audio/music_calm.ogg');
};

SIN.EpiloguePreload.prototype.create = function() {
  //loading screen will have a white background
  this.game.stage.backgroundColor = '#000';

  //scaling options
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.forceLandscape = true;
  this.scale.pageAlignHorizontally = true;
  //this.scale.updateLayout();

  //physics system
  this.game.physics.startSystem(Phaser.Physics.ARCADE);
  this.game.state.start('Epilogue');
};
