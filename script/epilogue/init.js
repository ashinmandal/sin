var SIN = SIN || {};

SIN.Game = (function (doc) {
  this.game = new Phaser.Game(1000, 600, Phaser.AUTO, 'content');
  this.game.state.add('Boot', SIN.Boot);
  this.game.state.add('EpiloguePreload', SIN.EpiloguePreload);
  this.game.state.add('Epilogue', SIN.Epilogue);
  this.game.state.start('Boot');
})(document);
