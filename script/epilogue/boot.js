var SIN = SIN || {};

SIN.Boot = function(){};

SIN.Boot.prototype = Object.create(Phaser.State.prototype);

SIN.Boot.prototype.preload = function() {
  //assets we'll use in the loading screen
  this.load.image('healthbar', 'assets/images/healthbar.png');
  this.load.image('logo', 'assets/images/logo.png');
};

SIN.Boot.prototype.create = function() {
  //loading screen will have a white background
  this.game.stage.backgroundColor = '#000';

  //scaling options
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.forceLandscape = true;
  this.scale.pageAlignHorizontally = true;
  //this.scale.updateLayout();

  //physics system
  this.game.physics.startSystem(Phaser.Physics.ARCADE);

  this.game.state.start('EpiloguePreload');
};
