var SIN = SIN || {};

SIN.Epilogue = function(){};

SIN.Epilogue.prototype = Object.create(Phaser.State.prototype);

SIN.Epilogue.prototype.create = function() {
  this.playMusic();
  this.epilogue1 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue1');
  this.epilogue1.anchor.setTo(0.5);
  this.epilogue1.alpha = 0;

  this.epilogue2 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue2');
  this.epilogue2.anchor.setTo(0.5);
  this.epilogue2.alpha = 0;

  this.epilogue3 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue3');
  this.epilogue3.anchor.setTo(0.5);
  this.epilogue3.alpha = 0;

  this.epilogue4 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue4');
  this.epilogue4.anchor.setTo(0.5);
  this.epilogue4.alpha = 0;

  this.epilogue5 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue5');
  this.epilogue5.anchor.setTo(0.5);
  this.epilogue5.alpha = 0;

  this.epilogue6 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue6');
  this.epilogue6.anchor.setTo(0.5);
  this.epilogue6.alpha = 0;

  this.epilogue7 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue7');
  this.epilogue7.anchor.setTo(0.5);
  this.epilogue7.alpha = 0;

  this.epilogue8 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue8');
  this.epilogue8.anchor.setTo(0.5);
  this.epilogue8.alpha = 0;

  this.epilogue9 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue9');
  this.epilogue9.anchor.setTo(0.5);
  this.epilogue9.alpha = 0;

  this.epilogue10 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'epilogue10');
  this.epilogue10.anchor.setTo(0.5);
  this.epilogue10.alpha = 0;

  this.game.time.events.add(250, this.showScene1, this );

  this.game.time.events.add(21400, this.showScene2, this );

  this.game.time.events.add(47600, this.showScene3, this );

  this.game.time.events.add(60000, this.showScene4, this );

  this.game.time.events.add(72000, this.showScene5, this );

  this.game.time.events.add(80000, this.showScene6, this );

  this.game.time.events.add(85000, this.showScene7, this );

  this.game.time.events.add(90000, this.showScene8, this );

  this.game.time.events.add(95000, this.showScene9, this );

  this.game.time.events.add(100000, this.showScene10, this );
};

SIN.Epilogue.prototype.playMusic = function() {
  this.music = this.game.add.audio('epilogue_dialogue');
  this.music.play();

  this.game.onPause.add(this.pauseMusic, this);
  this.game.onResume.add(this.resumeMusic, this);
};

SIN.Epilogue.prototype.pauseMusic = function() {
  this.music.pause();
};

SIN.Epilogue.prototype.resumeMusic = function() {
  this.music.resume();
};

SIN.Epilogue.prototype.playBGMusic = function() {
  this.bgMusic = this.game.add.audio('music_calm');
  this.bgMusic.play();

  this.game.onPause.add(this.pauseBGMusic, this);
  this.game.onResume.add(this.resumeBGMusic, this);
};

SIN.Epilogue.prototype.pauseBGMusic = function() {
  this.bgMusic.pause();
};

SIN.Epilogue.prototype.resumeBGMusic = function() {
  this.bgMusic.resume();
};

SIN.Epilogue.prototype.showScene1 = function() {
  this.epilogue1Alpha = this.game.add.tween(this.epilogue1).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
};

SIN.Epilogue.prototype.showScene2 = function() {
  this.epilogue1Alpha = this.game.add.tween(this.epilogue1).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue1Alpha.onComplete.add(function () {
    this.epilogue2Alpha = this.game.add.tween(this.epilogue2).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene3 = function() {
  this.epilogue2Alpha = this.game.add.tween(this.epilogue2).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue2Alpha.onComplete.add(function () {
    this.epilogue3Alpha = this.game.add.tween(this.epilogue3).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene4 = function() {
  this.epilogue3Alpha = this.game.add.tween(this.epilogue3).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue3Alpha.onComplete.add(function () {
    this.epilogue4Alpha = this.game.add.tween(this.epilogue4).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene5 = function() {
  this.epilogue4Alpha = this.game.add.tween(this.epilogue4).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue4Alpha.onComplete.add(function () {
    this.epilogue4Alpha = this.game.add.tween(this.epilogue5).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene6 = function() {
  this.epilogue5Alpha = this.game.add.tween(this.epilogue5).to({alpha: 0}, 1200, Phaser.Easing.Linear.None).start();
  this.epilogue5Alpha.onComplete.add(function () {
    this.epilogue6Alpha = this.game.add.tween(this.epilogue6).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene7 = function() {
  this.playBGMusic();
  this.epilogue6Alpha = this.game.add.tween(this.epilogue6).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue6Alpha.onComplete.add(function () {
    this.epilogue7Alpha = this.game.add.tween(this.epilogue7).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene8 = function() {
  this.epilogue7Alpha = this.game.add.tween(this.epilogue7).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue7Alpha.onComplete.add(function () {
    this.epilogue8Alpha = this.game.add.tween(this.epilogue8).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene9 = function() {
  this.epilogue8Alpha = this.game.add.tween(this.epilogue8).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue8Alpha.onComplete.add(function () {
    this.epilogue9Alpha = this.game.add.tween(this.epilogue9).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Epilogue.prototype.showScene10 = function() {
  this.epilogue9Alpha = this.game.add.tween(this.epilogue9).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.epilogue9Alpha.onComplete.add(function () {
    this.epilogue10Alpha = this.game.add.tween(this.epilogue10).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  }, this);
};
