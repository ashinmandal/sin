var SIN = SIN || {};

SIN.Degir = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'sin_spritesheet');
  this.game.add.existing(this);

  this.inputEnabled = true;

  this.deathAnimationComplete = false;

  this.spawnPointX = x;
  this.spawnPointY = y;

  this.game.physics.arcade.enable(this);
  this.body.gravity.y = 1000;

  this.animations.add('stand', Phaser.Animation.generateFrameNames('', 31, 38, '.png', 2), 10, true, false);
  this.animations.add('run', Phaser.Animation.generateFrameNames('', 1, 8, '.png', 2), 10, true, false);
  this.animations.add('jump', Phaser.Animation.generateFrameNames('', 9, 16, '.png', 2), 10, true, false);
  this.animations.add('death', Phaser.Animation.generateFrameNames('', 17, 30, '.png', 2), 10, true, false);
  this.animations.add('hang', Phaser.Animation.generateFrameNames('', 39, 49, '.png', 2), 8, true, false);
  this.animations.add('turn', Phaser.Animation.generateFrameNames('', 50, 56, '.png', 2), 10, true, false);

  this.anchor.setTo(0.5, 1);

  this.health = 10.0;
  this.maxHealth = 10;

  this.hanging = false;
  this.showedRestartText = false;

  //move player with cursor keys
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
  this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
  this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
};

SIN.Degir.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Degir.prototype.constructor = SIN.Degir;

SIN.Degir.prototype.update = function() {
  //only respond to keys and keep the speed if the player is alive
  if(this.inputEnabled) {
    if (!this.hanging) {
      if((this.cursors.right.isDown && this.cursors.up.isDown) || (this.rightKey.isDown && this.jumpKey.isDown)) {
        this.animations.play('jump');
        this.body.velocity.x = 300;
        this.scale.setTo(1);
      } else if((this.cursors.left.isDown && this.cursors.up.isDown) || (this.leftKey.isDown && this.jumpKey.isDown)) {
        this.animations.play('jump');
        this.body.velocity.x = -300;
        this.scale.setTo(-1, 1);
      } else if(this.cursors.right.isDown || this.rightKey.isDown) {
        if (this.body.blocked.down) {
          this.animations.play('run');
          this.scale.setTo(1);
        }
        this.body.velocity.x = 300;
      } else if(this.cursors.left.isDown || this.leftKey.isDown) {
        if (this.body.blocked.down) {
          this.animations.play('run');
          this.scale.setTo(-1, 1);
        }
        this.body.velocity.x = -300;
      } else if(this.cursors.up.isDown || this.jumpKey.isDown) {
        this.animations.play('jump');
        this.scale.setTo(1);
      } else if((!this.cursors.right.isDown || !this.cursors.left.isDown) || (!this.rightKey.isDown || !this.leftKey.isDown)) {
        this.body.velocity.x = 0;
        this.animations.play('stand');
        this.scale.setTo(1);
      }

      if(this.cursors.up.isDown || this.jumpKey.isDown) {
        this.playerJump();
      }

      if(this.x >= this.game.world.width) {
        if (this.game.state.current == 'Level1Game') {
          if(this.timer !== null) {
            game.time.events.remove(this.timer);
          }
          this.game.state.start('Level2Preload');
        } else if (this.game.state.current == 'Level2Game') {
          this.game.state.start('Level3Preload');
        } else if (this.game.state.current == 'Level3Game') {
          this.game.state.start('Level4Preload');
        } else {
          this.game.state.start('EpiloguePreload');
        }
      }
    }
  } else {
    this.die();
  }
};

SIN.Degir.prototype.playerJump = function() {
  if(this.body.blocked.down) {
    this.animations.play('jump');
    this.body.velocity.y -= 650;
  }
};

SIN.Degir.prototype.die = function() {
  this.inputEnabled = false;
  this.body.velocity.x = 0;
  this.body.velocity.y = 0;
  this.health = 10.0;
  this.animations.play('death', 10, false, false);
  this.animations.currentAnim.onComplete.add(function () {
    this.deathAnimationComplete = true;
  }, this);
};
