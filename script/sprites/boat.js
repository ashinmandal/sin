var SIN = SIN || {};

SIN.Boat = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'Boat');
  this.game.add.existing(this);
  this.game.physics.arcade.enable(this);
  this.body.customSeparateX = true;
  this.body.customSeparateY = true;
  this.body.allowGravity = false;
  this.body.immovable = true;
  this.body.velocity.x = 0;
  this.anchor.x = 0.5;
  this.playerLocked = false;
};

SIN.Boat.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Boat.prototype.constructor = SIN.Boat;
