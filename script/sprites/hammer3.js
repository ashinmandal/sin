var SIN = SIN || {};

SIN.Hammer3 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer3');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Hammer3.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer3.prototype.constructor = SIN.Hammer3;
