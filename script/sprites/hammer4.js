var SIN = SIN || {};

SIN.Hammer4 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer4');
  this.game.add.existing(this);
  this.game.physics.arcade.enable(this);
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Hammer4.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer4.prototype.constructor = SIN.Hammer4;
