var SIN = SIN || {};

SIN.SpikeWheel = function (game, startX, endX, y, duration) {
  Phaser.Sprite.call(this, game, startX, y, 'spike_wheel');
  this.game.physics.arcade.enable(this);
  this.body.allowGravity = false;
  this.body.immovable = true;
  if (startX > endX) {
    this.forward = false;
  } else {
    this.forward = true;
  }
  this.anchor.setTo(0.5);
  toTween = game.add.tween(this).to({x: endX}, duration, Phaser.Easing.Circular.None).start();
  fromTween = game.add.tween(this).to({x: startX}, duration, Phaser.Easing.Circular.None);
  toTween.chain(fromTween);
  fromTween.chain(toTween);
  toTween.onComplete.add(function () {
    this.forward = !this.forward;
  }, this);
  fromTween.onComplete.add(function () {
    this.forward = !this.forward;
  }, this);
};

SIN.SpikeWheel.prototype = Object.create(Phaser.Sprite.prototype);
SIN.SpikeWheel.prototype.constructor = SIN.SpikeWheel;

SIN.SpikeWheel.prototype.update = function() {
  if (this.forward) {
    this.angle += 2.5;
  } else {
    this.angle -= 2.5;
  }

};
