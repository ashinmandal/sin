var SIN = SIN || {};

SIN.HealthBar = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'healthbar');
  this.game.add.existing(this);
  //this.healthbar.scale.setTo(2);
  this.cropEnabled = true;
  this.fixedToCamera = true;
};

SIN.HealthBar.prototype = Object.create(Phaser.Sprite.prototype);
SIN.HealthBar.prototype.constructor = SIN.HealthBar;
