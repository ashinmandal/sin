var SIN = SIN || {};

SIN.Axe1 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe1');
  //this.body.immovable = true;
  //this.body.allowGravity = false;
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Axe1.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe1.prototype.constructor = SIN.Axe1;
