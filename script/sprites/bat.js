var SIN = SIN || {};

SIN.Bat = function (game, startX, endX, y, duration) {
  Phaser.Sprite.call(this, game, startX, y, 'bat');
  this.game.physics.arcade.enable(this);
  this.animations.add('fly', Phaser.Animation.generateFrameNames('', 1, 8, '.png', 1), 10, true, false);
  this.animations.play('fly');
  if (startX > endX) {
    this.back = -1;
  } else {
    this.back = 1;
  }

  this.anchor.setTo(0.5);
  this.scale.setTo((1 * this.back), 1);
  toTween = game.add.tween(this).to({x: endX}, duration, Phaser.Easing.Quadratic.None).start();
  fromTween = game.add.tween(this).to({x: startX}, duration, Phaser.Easing.Quadratic.None);
  toTween.chain(fromTween);
  fromTween.chain(toTween);
  toTween.onComplete.add(function () {
    this.scale.setTo((-1 * this.back), 1);
    this.body.setSize(42, 137);
  }, this);
  fromTween.onComplete.add(function () {
    this.scale.setTo((1 * this.back), 1);
    this.body.setSize(105, 137);
  }, this);
};

SIN.Bat.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Bat.prototype.constructor = SIN.Bat;

SIN.Bat.prototype.update = function() {
  this.animations.play('fly');
};
