var SIN = SIN || {};

SIN.Axe2 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe2');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Quartic.InOut, true, 0, Infinity, true);
};

SIN.Axe2.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe2.prototype.constructor = SIN.Axe2;
