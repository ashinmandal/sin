var SIN = SIN || {};

SIN.Hammer1 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer1');
  //this.body.immovable = true;
  //this.body.allowGravity = false;
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Hammer1.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer1.prototype.constructor = SIN.Hammer1;
