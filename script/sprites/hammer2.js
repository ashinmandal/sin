var SIN = SIN || {};

SIN.Hammer2 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer2');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Quartic.InOut, true, 0, Infinity, true);
};

SIN.Hammer2.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer2.prototype.constructor = SIN.Hammer2;
