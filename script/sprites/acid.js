var SIN = SIN || {};

SIN.Acid = function (game, x, duration) {
  Phaser.Sprite.call(this, game, x, 700, 'acid');
  this.game.physics.arcade.enable(this);
  tween = game.add.tween(this).to({y: -100}, duration, Phaser.Easing.Elastic.None, true, 0, Infinity);
  this.minus = 1;
  tween.onComplete.add(function () {
    if (Phaser.Math.chanceRoll(50)) {
      this.minus = -1;
    }
    this.body.x = x + (this.minus * Math.random() * 20);
    this.body.y = 700;
  }, this);
};

SIN.Acid.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Acid.prototype.constructor = SIN.Acid;
