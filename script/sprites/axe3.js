var SIN = SIN || {};

SIN.Axe3 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe3');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Axe3.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe3.prototype.constructor = SIN.Axe3;
