var SIN = SIN || {};

SIN.Pellet = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'pellet');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Circular.InOut, true, 0, Infinity, true);
};

SIN.Pellet.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Pellet.prototype.constructor = SIN.Pellet;
