var SIN = SIN || {};

SIN.Level2Game = function(){
  this.boat = [];
  this.healthbar = [];
  this.player = [];
  this.locked = false;
  this.lockCheck = false;
  this.letGoCheck = false;
  this.waitingTime = [];
  this.batLockedTo = [];
};

SIN.Level2Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level2Game.prototype.constructor = SIN.Level2Game;

SIN.Level2Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level2Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2000, 700, 'level2_bg_1');
  this.game.add.tileSprite(2000, 0, 2000, 700, 'level2_bg_2');
  this.game.add.tileSprite(4000, 0, 2000, 700, 'level2_bg_3');
  this.game.add.tileSprite(6000, 0, 2000, 700, 'level2_bg_4');
  this.game.add.tileSprite(8000, 0, 2000, 700, 'level2_bg_5');

  this.map = this.game.add.tilemap('level2');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Jungle_Spritesheet', 'Jungle_Spritesheet');
  this.map.addTilesetImage('Boat', 'Boat');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.waterLayer = this.map.createLayer('waterLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.treeLayer = this.map.createLayer('treeLayer');
  this.bigTreeLayer = this.map.createLayer('bigTreeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'waterLayer');
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'treeLayer');
  this.map.setCollisionBetween(1, 100000, true, 'bigTreeLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.waitingTime = game.time.now;
  this.locked = false;
  this.lockCheck = false;
  this.letGoCheck = false;
  this.dialogue2Said = false;

  this.batGroup = game.add.physicsGroup();
  this.bat_1 = new SIN.Bat(game, 1200, 1600, 50, 1000);
  this.batGroup.add(this.bat_1);
  this.bat_2 = new SIN.Bat(game, 2700, 3300, 150, 2000);
  this.batGroup.add(this.bat_2);
  this.bat_3 = new SIN.Bat(game, 3500, 4080, 150, 2000);
  this.batGroup.add(this.bat_3);
  this.bat_4 = new SIN.Bat(game, 4600, 5400, 150, 2000);
  this.batGroup.add(this.bat_4);

  this.isKeyCollected = false;
  this.createKey();

  this.pelletGroup = game.add.physicsGroup();

  this.pellet_1 = new SIN.Pellet(game, 6085, 750, 300, 1000);
  this.pelletGroup.add(this.pellet_1);
  this.pellet_2 = new SIN.Pellet(game, 6289, 300, 750, 1000);
  this.pelletGroup.add(this.pellet_2);
  this.pellet_3 = new SIN.Pellet(game, 7564, 750, 300, 1000);
  this.pelletGroup.add(this.pellet_3);
  this.pellet_4 = new SIN.Pellet(game, 7794, 300, 750, 1000);
  this.pelletGroup.add(this.pellet_4);
  this.pellet_5 = new SIN.Pellet(game, 8665, 750, 300, 800);
  this.pelletGroup.add(this.pellet_5);

  this.acidGroup = game.add.physicsGroup();

  this.acid_1 = new SIN.Acid(game, 4560, 1000);
  this.acidGroup.add(this.acid_1);
  this.acid_2 = new SIN.Acid(game, 5380, 1000);
  this.acidGroup.add(this.acid_2);
  this.acid_3 = new SIN.Acid(game, 8169, 1200);
  this.acidGroup.add(this.acid_3);
  this.acid_4 = new SIN.Acid(game, 8339, 1200);
  this.acidGroup.add(this.acid_4);

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 285, 450);
  this.height = 130;

  this.collected1 = this.game.add.sprite(20, 20, 'collected1');
  this.collected1.fixedToCamera = true;
  this.collected2 = this.game.add.sprite(20, 20, 'collected2');
  this.collected2.fixedToCamera = true;
  this.collected2.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);

  this.mistGroup = game.add.physicsGroup();
  this.disappearingMist = game.add.sprite(1984, 440, 'mist');
  this.mistGroup.add(this.disappearingMist);
  this.mistGroup.create(2700, 440, 'mist');
  this.mistGroup.create(3420, 440, 'mist');

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.letGoKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level2Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.waterLayer, this.dieAtOnce, null, this);
  this.game.physics.arcade.overlap(this.player, this.pelletGroup, this.dieAtOnce, null, this);
  this.game.physics.arcade.overlap(this.player, this.acidGroup, this.dieAtOnce, null, this);
  this.game.physics.arcade.collide(this.player, this.treeLayer);
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.overlap(this.player, this.key, this.fogDisappear, null, this);
  this.game.physics.arcade.overlap(this.player, this.batGroup, this.playerRopeHang, null, this);
  this.game.physics.arcade.overlap(this.player, this.mistGroup, this.playerInFog, null, this);

  if (this.locked && this.player.hanging) {
    this.player.body.x = this.batLockedTo.body.x;
    this.player.body.y = this.batLockedTo.body.y + 105;
  }

  if (this.letGoKey.isDown && this.locked && this.letGoCheck) {
    this.player.animations.play('jump');
    this.player.anchor.setTo(0.5, 1);
    this.player.body.gravity.y = 1000;
    this.locked = false;
    this.waitingTime = game.time.now + 1500;
    this.player.hanging = false;
  }

  if (this.waitingTime == game.time.now) {
    this.lockCheck = false;
    this.letGoCheck = false;
  }

  if (this.waitingTime < game.time.now) {
    this.lockCheck = false;
  }


  if (this.player.body.x >= 1800 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level2_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 9732) {
    this.player.hanging = true;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.player.animations.play('stand');
    this.game.time.events.add(Phaser.Timer.SECOND, this.decrease, this);
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;

    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.dialogue2Said = false;
    this.collected1.alpha = 1;
    this.collected2.alpha = 0;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
      this.disappearingMist = game.add.sprite(1984, 440, 'mist');
      this.mistGroup.add(this.disappearingMist);
    }
  }
};

SIN.Level2Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.collected1, 32, 32);
};

SIN.Level2Game.prototype.createKey = function() {
  this.key = game.add.sprite(2200, 130, 'key2');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 150}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level2Game.prototype.playerInFog = function() {
  //set to dead (this doesn't affect rendering)
  this.player.health -= 0.1;
  cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
  console.log("Killing degir slowly because he is in fog: ", cropWidth);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
  if (this.player.health <= 0) {
      this.player.die();
  }
};

SIN.Level2Game.prototype.playerRopeHang = function(player, bat) {
  if (!this.lockCheck) {
    this.locked = true;
    this.lockCheck = true;
  }
  if (this.locked) {
    this.player.hanging = true;
    this.player.animations.play('hang');
    this.player.anchor.setTo(0.5, 0);
    this.player.body.gravity.y = 0;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.batLockedTo = bat;
  }
  if (this.letGoKey.isUp) {
    this.letGoCheck = true;
  }
};

SIN.Level2Game.prototype.fogDisappear = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected1.alpha = 0;
    this.collected2.alpha = 1;
  }, this);
  var mistTween = this.game.add.tween(this.disappearingMist).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.disappearingMist.kill();
  }, this);
};

SIN.Level2Game.prototype.dieAtOnce = function() {
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level2Game.prototype.decrease = function() {
  if (this.player.height > 0) {
    this.player.crop(new Phaser.Rectangle(0, 0, this.player.width, this.height));
    this.height -= 10;
  }
  if (this.height < 0) {
    this.bgMusic.destroy();
    this.game.state.start('Level3Preload');
  }
};
