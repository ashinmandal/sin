var SIN = SIN || {};

SIN.Level2Preload = function(){};

SIN.Level2Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level2Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading2');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level2', 'assets/tilemaps/level2.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Jungle_Spritesheet', 'assets/images/level2/spritesheet.png');
  this.load.image('Boat', 'assets/images/level1/Boat.png');
  this.load.audio('level2_dialogue2', 'assets/audio/vo_degir_mist.ogg');

  this.load.image('level2_bg_1', 'assets/images/level2/bg_1.png');
  this.load.image('level2_bg_2', 'assets/images/level2/bg_2.png');
  this.load.image('level2_bg_3', 'assets/images/level2/bg_3.png');
  this.load.image('level2_bg_4', 'assets/images/level2/bg_4.png');
  this.load.image('level2_bg_5', 'assets/images/level2/bg_5.png');

  this.game.load.atlasJSONHash('bat', 'assets/images/level2/bat.png', 'assets/images/level2/bat.json');
  this.load.image('key2', 'assets/images/level2/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.image('collected1', 'assets/images/collected1.png');
  this.load.image('collected2', 'assets/images/collected2.png');
  this.load.image('mist', 'assets/images/level2/mist.png');
  this.load.image('pellet', 'assets/images/weapons/pellet.png');
  this.load.image('acid', 'assets/images/weapons/acid.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level2Preload.prototype.create = function() {
  this.game.state.start('Level2Game');
};
