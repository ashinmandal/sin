var SIN = SIN || {};

SIN.Level4Game = function(){
  this.healthbar = [];
  this.player = [];
  this.timeCheck = [];
};

SIN.Level4Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level4Game.prototype.constructor = SIN.Level2Game;

SIN.Level4Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level4Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2170, 700, 'level4_bg_1');
  this.game.add.tileSprite(2170, 0, 2170, 700, 'level4_bg_2');
  this.game.add.tileSprite(4340, 0, 2170, 700, 'level4_bg_3');
  this.game.add.tileSprite(6510, 0, 2170, 700, 'level4_bg_4');
  this.game.add.tileSprite(8680, 0, 2170, 700, 'level4_bg_5');
  this.game.add.tileSprite(10850, 0, 2170, 700, 'level4_bg_6');

  this.map = this.game.add.tilemap('level4');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Phase IV Spritesheet Foreground', 'Phase IV Spritesheet Foreground');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.pitLayer = this.map.createLayer('pitLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'pitLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.fireflies_1 = game.add.sprite(10717, 0, 'fireflies_1');
  this.game.add.tween(this.fireflies_1).to({x: 10757}, 1000, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
  this.fireflies_2 = game.add.sprite(10817, 0, 'fireflies_2');
  this.game.add.tween(this.fireflies_2).to({x: 10767}, 1000, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);

  this.spikeWheelGroup = game.add.physicsGroup();

  this.spike_1 = new SIN.SpikeWheel(game, 450, 1300, 460, 3000);
  this.spikeWheelGroup.add(this.spike_1);
  this.spike_3 = new SIN.SpikeWheel(game, 1920, 2420, 460, 2000);
  this.spikeWheelGroup.add(this.spike_3);
  this.spike_4 = new SIN.SpikeWheel(game, 4090, 4590, 460, 2000);
  this.spikeWheelGroup.add(this.spike_4);
  this.spike_5 = new SIN.SpikeWheel(game, 9796, 9927, 460, 800);
  this.spikeWheelGroup.add(this.spike_5);
  this.spike_6 = new SIN.SpikeWheel(game, 10258, 10127, 460, 800);
  this.spikeWheelGroup.add(this.spike_6);

  this.hammerGroup = game.add.physicsGroup();

  this.hammer1_1 = new SIN.Hammer1(game, 2940, -400, -25, 1250);
  this.hammerGroup.add(this.hammer1_1);
  this.hammer1_2 = new SIN.Hammer1(game, 8380, -400, 10, 1250);
  this.hammerGroup.add(this.hammer1_2);

  this.hammer2_1 = new SIN.Hammer2(game, 3675, -400, -60, 950);
  this.hammerGroup.add(this.hammer2_1);

  this.hammer3_1 = new SIN.Hammer3(game, 5664, -400, -60, 950);
  this.hammerGroup.add(this.hammer3_1);
  this.hammer3_2 = new SIN.Hammer3(game, 7130, -400, -60, 950);
  this.hammerGroup.add(this.hammer3_2);

  this.hammer4 = new SIN.Hammer4(game, 10990, -400, 10, 950);
  this.hammerGroup.add(this.hammer4);

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;

  this.daggerGroup = game.add.physicsGroup();

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 100, 160);
  //this.player = new SIN.Degir(game, 11400, 160);

  this.collected3 = this.game.add.sprite(20, 20, 'collected3');
  this.collected3.fixedToCamera = true;
  this.collected4 = this.game.add.sprite(20, 20, 'collected4');
  this.collected4.fixedToCamera = true;
  this.collected4.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level4Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.collide(this.player, this.pitLayer, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.player, this.spikeWheelGroup, this.playerHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.hammerGroup, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.knife, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);

  if (this.player.body.x > 2445 && this.player.body.x < 3500 && game.time.now - this.timeCheck > 4000) {
    this.knife = this.daggerGroup.create((this.player.body.x + 900), 360, 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -600;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x > 5750 && this.player.body.x < 7100 && game.time.now - this.timeCheck > 2000) {
    this.knife = this.daggerGroup.create((this.player.body.x + 900), 490, 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -600;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 2620 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level4_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 9500 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level4_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 12359) {
    this.player.hanging = true;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.player.animations.play('turn', 2, false, false);
    this.player.animations.currentAnim.onComplete.add(function () {
      this.lastTween = this.game.add.tween(this.player).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
      this.lastTween.onComplete.add(function () {
        this.bgMusic.destroy();
        window.location.replace("epilogue.html");
        //this.game.state.start('EpiloguePreload');
      }, this);
    }, this);
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected3.alpha = 1;
    this.collected4.alpha = 0;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
    }
  }
};

SIN.Level4Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level4Game.prototype.resetLevel = function() {
  this.daggerGroup.forEach(function(dag) {
    dag.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level4Game.prototype.createKey = function() {
  this.key = game.add.sprite(9998, 194, 'key4');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 224}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level4Game.prototype.playerHit = function(player, spikeWheelGroup) {

  this.player.health -= 0.1;
  if (player.body.blocked.right) {
    this.player.body.x -= 30;
  } else if (player.body.blocked.left) {
    this.player.body.x += 30;
  }

  cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
  console.log("Health bar width at playerHit() - collision with spikewheel: ", cropWidth);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
  if (this.player.health <= 0) {
      this.resetLevel();
  }

};

SIN.Level4Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected3.alpha = 0;
    this.collected4.alpha = 1;
  }, this);
};
