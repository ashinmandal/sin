var SIN = SIN || {};

SIN.Level4Preload = function(){};

SIN.Level4Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level4Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading4');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level4', 'assets/tilemaps/level4.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Phase IV Spritesheet Foreground', 'assets/images/level4/spritesheet.png');

  this.load.image('level4_bg_1', 'assets/images/level4/bg_1.png');
  this.load.image('level4_bg_2', 'assets/images/level4/bg_2.png');
  this.load.image('level4_bg_3', 'assets/images/level4/bg_3.png');
  this.load.image('level4_bg_4', 'assets/images/level4/bg_4.png');
  this.load.image('level4_bg_5', 'assets/images/level4/bg_5.png');
  this.load.image('level4_bg_6', 'assets/images/level4/bg_6.png');
  this.load.image('fireflies_1', 'assets/images/level4/fireflies_1.png');
  this.load.image('fireflies_2', 'assets/images/level4/fireflies_2.png');

  this.load.image('key4', 'assets/images/level4/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.audio('level4_dialogue1', 'assets/audio/vo_sin_laugh.ogg');
  this.load.audio('level4_dialogue2', 'assets/audio/vo_degir_key4.ogg');
  this.load.image('collected3', 'assets/images/collected3.png');
  this.load.image('collected4', 'assets/images/collected4.png');

  this.load.image('spike_wheel', 'assets/images/weapons/spike_wheel.png');
  this.load.image('hammer1', 'assets/images/weapons/hammer1.png');
  this.load.image('hammer2', 'assets/images/weapons/hammer2.png');
  this.load.image('hammer3', 'assets/images/weapons/hammer3.png');
  this.load.image('hammer4', 'assets/images/weapons/hammer4.png');
  this.load.image('knife', 'assets/images/weapons/knife.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level4Preload.prototype.create = function() {
  this.game.state.start('Level4Game');
};
