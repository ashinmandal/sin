var SIN = SIN || {};

SIN.ProloguePreload = function(){};

SIN.ProloguePreload.prototype = Object.create(Phaser.State.prototype);

SIN.ProloguePreload.prototype.preload = function() {
  //show loading screen
  this.logo = this.game.add.sprite(this.game.world.centerX, (this.game.world.centerY - 20), 'logo');
  this.logo.anchor.setTo(0.5);

  this.preloadBar = this.add.sprite((this.game.world.centerX - 60), (this.game.world.centerY + 100), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);
  this.load.image('prologue1', 'assets/images/prologue/1.png');
  this.load.image('prologue2', 'assets/images/prologue/2.png');
  this.load.image('prologue3', 'assets/images/prologue/3.png');
  this.load.image('prologue4', 'assets/images/prologue/4.png');
  this.load.image('prologue5', 'assets/images/prologue/5.png');
  this.load.image('esc', 'assets/images/prologue/esc.png');
  this.load.audio('prologue_dialogue', 'assets/audio/vo_sin_prologue.ogg');
  this.load.audio('prologue_degir_dialogue', 'assets/audio/vo_degir_prologue.ogg');
};

SIN.ProloguePreload.prototype.create = function() {
  this.game.state.start('Prologue');
};
