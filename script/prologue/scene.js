var SIN = SIN || {};

SIN.Prologue = function(){};

SIN.Prologue.prototype = Object.create(Phaser.State.prototype);

SIN.Prologue.prototype.create = function() {
  this.playMusic();
  this.prologue1 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue1');
  this.prologue1.scale.setTo(0.5);
  this.prologue1.anchor.setTo(0.5);
  this.prologue1.alpha = 0;

  this.prologue2 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue2');
  this.prologue2.anchor.setTo(0.5);
  this.prologue2.alpha = 0;

  this.prologue3 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue3');
  this.prologue3.anchor.setTo(0.5);
  this.prologue3.alpha = 0;

  this.prologue4 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue4');
  this.prologue4.anchor.setTo(0.5);
  this.prologue4.alpha = 0;

  this.prologue5 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue5');
  this.prologue5.anchor.setTo(0.5);
  this.prologue5.alpha = 0;

  this.esc = this.game.add.sprite(this.game.world.width - 100, this.game.world.height - 50, 'esc');

  this.game.time.events.add(250, this.showInitial, this );

  this.game.time.events.add(25250, this.showScene3, this );

  this.game.time.events.add(44060, this.showScene4, this );

  this.game.time.events.add(48400, this.showLastScene, this );

  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.skipKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);

  // this.game.time.events.add(9250, this.showStory, this );
  // this.game.time.events.add(11500, this.translateCanvas, this );
  // this.game.time.events.add(70000, this.showInfo, this );
};

SIN.Prologue.prototype.update = function() {
  if (this.skipKey.isDown) {
    this.music.destroy();
    this.game.state.start('Level1Preload');
  }
};

SIN.Prologue.prototype.playMusic = function() {
  this.music = this.game.add.audio('prologue_dialogue');
  this.music.play();

  this.game.onPause.add(this.pauseMusic, this);
  this.game.onResume.add(this.resumeMusic, this);
};

SIN.Prologue.prototype.pauseMusic = function() {
  this.music.pause();
};

SIN.Prologue.prototype.resumeMusic = function() {
  this.music.resume();
};

SIN.Prologue.prototype.playDegir = function() {
  this.degir = this.game.add.audio('prologue_degir_dialogue');
  this.degir.play();

  this.degir.onPause.add(this.pauseDegir, this);
  this.degir.onResume.add(this.resumeDegir, this);
};

SIN.Prologue.prototype.pauseDegir = function() {
  this.degir.pause();
};

SIN.Prologue.prototype.resumeDegir = function() {
  this.degir.resume();
};

SIN.Prologue.prototype.showInitial = function() {
  this.prologue1Alpha = this.game.add.tween(this.prologue1).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  this.prologue1Alpha.onComplete.add(function () {
    this.prologue1Scale = this.game.add.tween(this.prologue1.scale).to({x: 0.7, y: 0.7}, 7000, Phaser.Easing.Linear.None).start();
    this.prologue1Scale.onComplete.add(function () {
      this.prologue1Alpha = this.game.add.tween(this.prologue1).to({alpha: 0}, 500, Phaser.Easing.Linear.None).start();
      this.prologue1Alpha.onComplete.add(function () {
        this.prologue2Alpha = this.game.add.tween(this.prologue2).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
      }, this);
    }, this);
  }, this);
};

SIN.Prologue.prototype.showScene3 = function() {
  this.prologue3Alpha = this.game.add.tween(this.prologue3).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
};

SIN.Prologue.prototype.showScene4 = function() {
  this.prologue2Alpha = this.game.add.tween(this.prologue2).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue3Alpha = this.game.add.tween(this.prologue3).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue3Alpha.onComplete.add(function () {
    this.prologue4Alpha = this.game.add.tween(this.prologue4).to({alpha: 1}, 200, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Prologue.prototype.showLastScene = function() {
  this.prologue4Alpha = this.game.add.tween(this.prologue4).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue5Alpha = this.game.add.tween(this.prologue5).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  this.playDegir();
  this.degir.onStop.addOnce(function() {
    this.prologue5Alpha = this.game.add.tween(this.prologue5).to({alpha: 0}, 1500, Phaser.Easing.Linear.None).start();
    this.prologue5Alpha.onComplete.add(function () {
      this.game.state.start('Level1Preload');
    }, this);
  }, this);
};
