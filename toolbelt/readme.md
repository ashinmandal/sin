# SIN Docker Setup

<br />

### Prerequisites
---
1. Repository cloned on local
2. Docker installed and working

<br />

### Steps
---

> pwd = sin repo root

1. Build Image
<br />**$** `docker build -t sin-i:1.0.1 toolbelt`

2. Deploy Image
<br />**$** `docker run --name sin-c -p 9999:80 -d -v $(pwd):/usr/share/nginx/html sin-i:1.0.1`

3. Jump In
<br />**$** `docker exec -i -t sin-c /bin/bash`

4. Install Node Dependencies
<br />**#** `npm install`



<br />

### Recommendations
---
```bash
alias sins='docker start sin-c'
alias sinstp='docker stop sin-c'
alias sinssh='docker exec -i -t sin-c /bin/bash'
```
