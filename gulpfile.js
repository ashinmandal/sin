// grab our packages
var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

// set entry & exit points
var jsDest = 'build/';

var gameFiles = [ '!script/epilogue/*.*',
                  '!script/app.js',
                  'script/**/*.js'
                ];

var epilogueFiles = [ 'script/epilogue/boot.js',
                      'script/epilogue/preload.js',
                      'script/epilogue/scene.js'
                    ];

// define the default task and add the watch task to it
gulp.task('default', [ 'watch', 'build' ]);

// build has two sub-tasks
gulp.task('build', [ 'build-game', 'build-epilogue' ]);

// define the build task to concat and minify js
gulp.task('build-game', function() {
    return gulp.src(gameFiles)
        .pipe(concat('master.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('master.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});

gulp.task('build-epilogue', function() {
    return gulp.src(epilogueFiles)
        .pipe(concat('epilogue.master.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('epilogue.master.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});

// configure the jshint task
gulp.task('jshint', function() {
    return gulp.src(jsFiles)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
    gulp.watch(jsFiles, ['jshint']);
});
