# SIN

SIN is an independent 2D game built with Phaser (phaser.io).

The game is available online at this link - <a href="http://ashinmandal.com/sin" target="_blank">ashinmandal.com/sin</a>

### Commands

- `npm run gulp` to build minified JS file and watch for changes
- `npm run serve` to serve the game on `localhost:9999`

> Use docker toolbelt if you are on Windows
