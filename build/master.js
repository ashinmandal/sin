var SIN = SIN || {};

SIN.Level1Boot = function(){};

SIN.Level1Boot.prototype = Object.create(Phaser.State.prototype);

SIN.Level1Boot.prototype.preload = function() {
  //assets we'll use in the loading screen
  this.load.image('healthbar', 'assets/images/healthbar.png');
  this.load.image('logo', 'assets/images/logo.png');
  this.load.image('loading1', 'assets/images/level1/loading1.png');
  this.load.image('loading2', 'assets/images/level2/loading2.png');
  this.load.image('loading3', 'assets/images/level3/loading3.png');
  this.load.image('loading4', 'assets/images/level4/loading4.png');
};

SIN.Level1Boot.prototype.create = function() {
  //loading screen will have a white background
  this.game.stage.backgroundColor = '#000';

  //scaling options
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.forceLandscape = true;
  this.scale.pageAlignHorizontally = true;
  //this.scale.updateLayout();

  //physics system
  this.game.physics.startSystem(Phaser.Physics.ARCADE);

  this.game.state.start('ProloguePreload');
};

var SIN = SIN || {};

SIN.Level1Game = function(){
  this.locked = [];
  this.lockedTo = [];
  this.boat = [];
  this.healthbar = [];
  this.player = [];
  this.boatStarted = [];
  this.timeCheck = [];
};

SIN.Level1Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level1Game.prototype.constructor = SIN.Level1Game;

SIN.Level1Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
  this.locked = false;
  this.lockedTo = null;
  this.isBoatAnchored = true;
};

SIN.Level1Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 1750, 700, 'level1_bg_1');
  this.game.add.tileSprite(1750, 0, 1750, 700, 'level1_bg_2');
  this.game.add.tileSprite(3500, 0, 1750, 700, 'level1_bg_3');
  this.map = this.game.add.tilemap('level1');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Spritesheet_35px', 'Spritesheet_35px');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.rockLayer = this.map.createLayer('rockLayer');
  this.waterLayer = this.map.createLayer('waterLayer');
  this.barricadeLayer = this.map.createLayer('barricadeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'barricadeLayer');
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'rockLayer');
  this.map.setCollisionBetween(1, 100000, true, 'waterLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;


  // creating custom sprite objects
  this.boat = new SIN.Boat(game, 3720, 525);
  this.player = new SIN.Degir(game, 100, 260);
  this.healthbar = new SIN.HealthBar(game, 95, 30);

  this.collected0 = this.game.add.sprite(20, 20, 'collected0');
  this.collected0.fixedToCamera = true;
  this.collected1 = this.game.add.sprite(20, 20, 'collected1');
  this.collected1.fixedToCamera = true;
  this.collected1.alpha = 0;

  this.knifeGroup = game.add.physicsGroup();
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);

  this.bgMusic = this.game.add.audio('music_calm');
  this.bgMusic.loopFull(0.5);
};

SIN.Level1Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.barricadeLayer, this.playerHit, null, this);
  this.game.physics.arcade.collide(this.player, this.rockLayer, this.playerRockHit, null, this);
  this.game.physics.arcade.collide(this.player, this.waterLayer, this.playerRockHit, null, this);
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.collide(this.player, this.boat, this.playerBoatHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.knife, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.boat, this.waterLayer);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);

  if (this.locked) {
    this.checkLock();
  }

  if (this.locked && (this.jumpKey.isDown || this.cursors.up.isDown)) {
    this.cancelLock();
    this.player.body.velocity.y -= 650;
  }

  if (this.player.body.x >= 3350 && this.isBoatAnchored === false) {
    this.isBoatAnchored = true;
    this.boat.body.velocity.x = 80;
  }

  if (this.player.body.x > 500 && this.player.body.x < 3000 && game.time.now - this.timeCheck > 5000) {
    this.knife = this.knifeGroup.create((this.player.body.x + 900), (350 + Math.random() * 80), 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 2300 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level1_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 2960 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level1_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 5200) {
    this.bgMusic.destroy();
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected0.alpha = 1;
    this.collected1.alpha = 0;

    this.dialogue1Said = false;
    this.dialogue2Said = false;

    this.boat.body.x = 3720;
    this.boat.body.y = 525;
    this.boat.body.velocity.x = 0;
    this.isBoatAnchored = true;

    if (this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
    }
  }
};

SIN.Level1Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level1Game.prototype.resetLevel = function() {
  this.knifeGroup.forEach(function(kni) {
    kni.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level1Game.prototype.createKey = function() {
  this.key = game.add.sprite(2430, 42, 'key1');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 62}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level1Game.prototype.playerHit = function(player, barricadeLayer) {
  //if hits on the right side, die
  if(player.body.blocked.right) {
    //set to dead (this doesn't affect rendering)
    this.player.health -= 1;
    this.player.body.x -= 30;
    cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
    console.log("Health bar width at playerHit() - collision with barricadeLayer: ", cropWidth);
    this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
    if (this.player.health <= 0) {
        this.resetLevel();
    }
  }
};

SIN.Level1Game.prototype.playerRockHit = function(player, rockLayer) {
  //if hits on the right side, die
  if((this.player.body.blocked.down || this.player.body.blocked.right) && this.locked === false) {
    this.resetLevel();
  }
};

SIN.Level1Game.prototype.checkLock = function() {
  this.player.body.velocity.y = 0;
  //  If the player has walked off either side of the platform then they're no longer locked to it
  if (this.player.body.right < this.lockedTo.body.x || this.player.body.x > this.lockedTo.body.right)
  {
      this.cancelLock();
  }
};

SIN.Level1Game.prototype.cancelLock = function() {
  this.locked = false;
};

SIN.Level1Game.prototype.preRender = function() {
  if (this.game.paused)
  {
      //  Because preRender still runs even if your game pauses!
      return;
  }

  if (this.locked)
  {
      this.player.x += this.lockedTo.deltaX;
      this.player.y = this.lockedTo.y + 100;

      if (this.player.body.velocity.x !== 0)
      {
          this.player.body.velocity.y = 0;
      }
  }
};

SIN.Level1Game.prototype.playerBoatHit = function(player, boat) {
  if (!this.locked && player.body.velocity.y > 0)
  {
      this.locked = true;
      this.lockedTo = boat;
      boat.playerLocked = true;

      player.body.velocity.y = 0;
  }
};

SIN.Level1Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected0.alpha = 0;
    this.collected1.alpha = 1;
    this.isBoatAnchored = false;
  }, this);
};

var SIN = SIN || {};

SIN.Level1Preload = function(){};

SIN.Level1Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level1Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading1');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level1', 'assets/tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Spritesheet_35px', 'assets/images/level1/spritesheet.png');
  this.load.image('Boat', 'assets/images/level1/Boat.png');
  this.load.image('level1_bg_1', 'assets/images/level1/bg_1.png');
  this.load.image('level1_bg_2', 'assets/images/level1/bg_2.png');
  this.load.image('level1_bg_3', 'assets/images/level1/bg_3.png');

  this.load.audio('music_calm', 'assets/audio/music_calm.ogg');
  this.load.audio('level1_dialogue1', 'assets/audio/vo_degir_key1.ogg');
  this.load.audio('level1_dialogue2', 'assets/audio/vo_degir_boat.ogg');

  this.load.image('key1', 'assets/images/level1/key.png');
  this.load.image('collected0', 'assets/images/collected0.png');
  this.load.image('collected1', 'assets/images/collected1.png');

  this.load.image('knife', 'assets/images/weapons/knife.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level1Preload.prototype.create = function() {
  this.game.state.start('Level1Game');
};

var SIN = SIN || {};

SIN.Level2Game = function(){
  this.boat = [];
  this.healthbar = [];
  this.player = [];
  this.locked = false;
  this.lockCheck = false;
  this.letGoCheck = false;
  this.waitingTime = [];
  this.batLockedTo = [];
};

SIN.Level2Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level2Game.prototype.constructor = SIN.Level2Game;

SIN.Level2Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level2Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2000, 700, 'level2_bg_1');
  this.game.add.tileSprite(2000, 0, 2000, 700, 'level2_bg_2');
  this.game.add.tileSprite(4000, 0, 2000, 700, 'level2_bg_3');
  this.game.add.tileSprite(6000, 0, 2000, 700, 'level2_bg_4');
  this.game.add.tileSprite(8000, 0, 2000, 700, 'level2_bg_5');

  this.map = this.game.add.tilemap('level2');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Jungle_Spritesheet', 'Jungle_Spritesheet');
  this.map.addTilesetImage('Boat', 'Boat');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.waterLayer = this.map.createLayer('waterLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.treeLayer = this.map.createLayer('treeLayer');
  this.bigTreeLayer = this.map.createLayer('bigTreeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'waterLayer');
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'treeLayer');
  this.map.setCollisionBetween(1, 100000, true, 'bigTreeLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.waitingTime = game.time.now;
  this.locked = false;
  this.lockCheck = false;
  this.letGoCheck = false;
  this.dialogue2Said = false;

  this.batGroup = game.add.physicsGroup();
  this.bat_1 = new SIN.Bat(game, 1200, 1600, 50, 1000);
  this.batGroup.add(this.bat_1);
  this.bat_2 = new SIN.Bat(game, 2700, 3300, 150, 2000);
  this.batGroup.add(this.bat_2);
  this.bat_3 = new SIN.Bat(game, 3500, 4080, 150, 2000);
  this.batGroup.add(this.bat_3);
  this.bat_4 = new SIN.Bat(game, 4600, 5400, 150, 2000);
  this.batGroup.add(this.bat_4);

  this.isKeyCollected = false;
  this.createKey();

  this.pelletGroup = game.add.physicsGroup();

  this.pellet_1 = new SIN.Pellet(game, 6085, 750, 300, 1000);
  this.pelletGroup.add(this.pellet_1);
  this.pellet_2 = new SIN.Pellet(game, 6289, 300, 750, 1000);
  this.pelletGroup.add(this.pellet_2);
  this.pellet_3 = new SIN.Pellet(game, 7564, 750, 300, 1000);
  this.pelletGroup.add(this.pellet_3);
  this.pellet_4 = new SIN.Pellet(game, 7794, 300, 750, 1000);
  this.pelletGroup.add(this.pellet_4);
  this.pellet_5 = new SIN.Pellet(game, 8665, 750, 300, 800);
  this.pelletGroup.add(this.pellet_5);

  this.acidGroup = game.add.physicsGroup();

  this.acid_1 = new SIN.Acid(game, 4560, 1000);
  this.acidGroup.add(this.acid_1);
  this.acid_2 = new SIN.Acid(game, 5380, 1000);
  this.acidGroup.add(this.acid_2);
  this.acid_3 = new SIN.Acid(game, 8169, 1200);
  this.acidGroup.add(this.acid_3);
  this.acid_4 = new SIN.Acid(game, 8339, 1200);
  this.acidGroup.add(this.acid_4);

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 285, 450);
  this.height = 130;

  this.collected1 = this.game.add.sprite(20, 20, 'collected1');
  this.collected1.fixedToCamera = true;
  this.collected2 = this.game.add.sprite(20, 20, 'collected2');
  this.collected2.fixedToCamera = true;
  this.collected2.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);

  this.mistGroup = game.add.physicsGroup();
  this.disappearingMist = game.add.sprite(1984, 440, 'mist');
  this.mistGroup.add(this.disappearingMist);
  this.mistGroup.create(2700, 440, 'mist');
  this.mistGroup.create(3420, 440, 'mist');

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.letGoKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level2Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.waterLayer, this.dieAtOnce, null, this);
  this.game.physics.arcade.overlap(this.player, this.pelletGroup, this.dieAtOnce, null, this);
  this.game.physics.arcade.overlap(this.player, this.acidGroup, this.dieAtOnce, null, this);
  this.game.physics.arcade.collide(this.player, this.treeLayer);
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.overlap(this.player, this.key, this.fogDisappear, null, this);
  this.game.physics.arcade.overlap(this.player, this.batGroup, this.playerRopeHang, null, this);
  this.game.physics.arcade.overlap(this.player, this.mistGroup, this.playerInFog, null, this);

  if (this.locked && this.player.hanging) {
    this.player.body.x = this.batLockedTo.body.x;
    this.player.body.y = this.batLockedTo.body.y + 105;
  }

  if (this.letGoKey.isDown && this.locked && this.letGoCheck) {
    this.player.animations.play('jump');
    this.player.anchor.setTo(0.5, 1);
    this.player.body.gravity.y = 1000;
    this.locked = false;
    this.waitingTime = game.time.now + 1500;
    this.player.hanging = false;
  }

  if (this.waitingTime == game.time.now) {
    this.lockCheck = false;
    this.letGoCheck = false;
  }

  if (this.waitingTime < game.time.now) {
    this.lockCheck = false;
  }


  if (this.player.body.x >= 1800 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level2_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 9732) {
    this.player.hanging = true;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.player.animations.play('stand');
    this.game.time.events.add(Phaser.Timer.SECOND, this.decrease, this);
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;

    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.dialogue2Said = false;
    this.collected1.alpha = 1;
    this.collected2.alpha = 0;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
      this.disappearingMist = game.add.sprite(1984, 440, 'mist');
      this.mistGroup.add(this.disappearingMist);
    }
  }
};

SIN.Level2Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.collected1, 32, 32);
};

SIN.Level2Game.prototype.createKey = function() {
  this.key = game.add.sprite(2200, 130, 'key2');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 150}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level2Game.prototype.playerInFog = function() {
  //set to dead (this doesn't affect rendering)
  this.player.health -= 0.1;
  cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
  console.log("Killing degir slowly because he is in fog: ", cropWidth);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
  if (this.player.health <= 0) {
      this.player.die();
  }
};

SIN.Level2Game.prototype.playerRopeHang = function(player, bat) {
  if (!this.lockCheck) {
    this.locked = true;
    this.lockCheck = true;
  }
  if (this.locked) {
    this.player.hanging = true;
    this.player.animations.play('hang');
    this.player.anchor.setTo(0.5, 0);
    this.player.body.gravity.y = 0;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.batLockedTo = bat;
  }
  if (this.letGoKey.isUp) {
    this.letGoCheck = true;
  }
};

SIN.Level2Game.prototype.fogDisappear = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected1.alpha = 0;
    this.collected2.alpha = 1;
  }, this);
  var mistTween = this.game.add.tween(this.disappearingMist).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.disappearingMist.kill();
  }, this);
};

SIN.Level2Game.prototype.dieAtOnce = function() {
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level2Game.prototype.decrease = function() {
  if (this.player.height > 0) {
    this.player.crop(new Phaser.Rectangle(0, 0, this.player.width, this.height));
    this.height -= 10;
  }
  if (this.height < 0) {
    this.bgMusic.destroy();
    this.game.state.start('Level3Preload');
  }
};

var SIN = SIN || {};

SIN.Level2Preload = function(){};

SIN.Level2Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level2Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading2');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level2', 'assets/tilemaps/level2.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Jungle_Spritesheet', 'assets/images/level2/spritesheet.png');
  this.load.image('Boat', 'assets/images/level1/Boat.png');
  this.load.audio('level2_dialogue2', 'assets/audio/vo_degir_mist.ogg');

  this.load.image('level2_bg_1', 'assets/images/level2/bg_1.png');
  this.load.image('level2_bg_2', 'assets/images/level2/bg_2.png');
  this.load.image('level2_bg_3', 'assets/images/level2/bg_3.png');
  this.load.image('level2_bg_4', 'assets/images/level2/bg_4.png');
  this.load.image('level2_bg_5', 'assets/images/level2/bg_5.png');

  this.game.load.atlasJSONHash('bat', 'assets/images/level2/bat.png', 'assets/images/level2/bat.json');
  this.load.image('key2', 'assets/images/level2/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.image('collected1', 'assets/images/collected1.png');
  this.load.image('collected2', 'assets/images/collected2.png');
  this.load.image('mist', 'assets/images/level2/mist.png');
  this.load.image('pellet', 'assets/images/weapons/pellet.png');
  this.load.image('acid', 'assets/images/weapons/acid.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level2Preload.prototype.create = function() {
  this.game.state.start('Level2Game');
};

var SIN = SIN || {};

SIN.Level3Game = function(){
  this.healthbar = [];
  this.player = [];
  this.height = [];
  this.down = [];
  this.wall = [];
  this.timeCheck = [];
  this.isWallEnabled = [];
};

SIN.Level3Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level3Game.prototype.constructor = SIN.Level2Game;

SIN.Level3Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level3Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2002, 700, 'level3_bg_1');
  this.game.add.tileSprite(2002, 0, 2002, 700, 'level3_bg_2');
  this.game.add.tileSprite(4004, 0, 2002, 700, 'level3_bg_3');
  this.game.add.tileSprite(6006, 0, 2002, 700, 'level3_bg_4');
  this.game.add.tileSprite(8008, 0, 2002, 700, 'level3_bg_5');

  this.map = this.game.add.tilemap('level3');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Phase III Spritesheet', 'Phase III Spritesheet');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.metalGroundLayer = this.map.createLayer('metalGroundLayer');
  this.lavaPoolLayer = this.map.createLayer('lavaPoolLayer');
  this.barrelLayer = this.map.createLayer('barrelLayer');
  this.barricadeLayer = this.map.createLayer('barricadeLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'metalGroundLayer');
  this.map.setCollisionBetween(1, 100000, true, 'lavaPoolLayer');
  this.map.setCollisionBetween(1, 100000, true, 'barrelLayer');
  this.map.setCollisionBetween(1, 100000, true, 'barricadeLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.wall = game.add.sprite(9000, 630, 'wall');
  this.game.physics.arcade.enable(this.wall);
  this.wall.cropEnabled = true;
  this.wall.anchor.setTo(0.5,1);
  this.height = 490;
  this.down = true;
  this.isWallEnabled = false;

  // Creating weapons for Level 3
  this.axeGroup = game.add.physicsGroup();

  this.axe1_1 = new SIN.Axe1(game, 1290, -400, 10, 850);
  this.axeGroup.add(this.axe1_1);

  this.axe1_2 = new SIN.Axe1(game, 2300, -400, 10, 850);
  this.axeGroup.add(this.axe1_2);

  this.axe1_3 = new SIN.Axe1(game, 3360, -400, -60, 900);
  this.axeGroup.add(this.axe1_3);

  this.axe2_1 = new SIN.Axe2(game, 4580, -400, 10, 950);
  this.axeGroup.add(this.axe2_1);

  this.axe2_2 = new SIN.Axe2(game, 5140, -400, 10, 950);
  this.axeGroup.add(this.axe2_2);

  this.axe2_3 = new SIN.Axe2(game, 5700, -400, 10, 950);
  this.axeGroup.add(this.axe2_3);

  this.axe3_1 = new SIN.Axe3(game, 6730, -400, 10, 850);
  this.axeGroup.add(this.axe3_1);

  this.axe3_1 = new SIN.Axe3(game, 7370, -400, 10, 850);
  this.axeGroup.add(this.axe3_1);

  this.axe3_3 = new SIN.Axe3(game, 8750, -400, 10, 850);
  this.axeGroup.add(this.axe3_3);

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;

  this.fireballGroup = game.add.physicsGroup();

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 100, 260);
  //this.player = new SIN.Degir(game, 10000, 160);

  this.collected2 = this.game.add.sprite(20, 20, 'collected2');
  this.collected2.fixedToCamera = true;
  this.collected3 = this.game.add.sprite(20, 20, 'collected3');
  this.collected3.fixedToCamera = true;
  this.collected3.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level3Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.metalGroundLayer);
  this.game.physics.arcade.collide(this.player, this.lavaPoolLayer, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.player, this.barrelLayer);
  this.game.physics.arcade.collide(this.player, this.barricadeLayer, this.playerHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.axeGroup, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.fireball, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.wall, this.playerWallHit, null, this);

  if (this.isWallEnabled) {
    if (this.down) {
      this.game.time.events.add(Phaser.Timer.SECOND * 0.8, this.decrease, this);
    } else {
      this.game.time.events.add(Phaser.Timer.SECOND * 0.8, this.increase, this);
    }
  }

  if (this.player.body.x > 2695 && this.player.body.x < 3535 && game.time.now - this.timeCheck > 3000) {
    this.fireball = this.fireballGroup.create((this.player.body.x + 900), 70, 'fireball');
    this.fireball.checkWorldBounds = true;
    this.fireball.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.fireball);
    this.fireball.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x > 6335 && this.player.body.x < 7640 && game.time.now - this.timeCheck > 4000) {
    this.fireball = this.fireballGroup.create((this.player.body.x + 900), 490, 'fireball');
    this.fireball.checkWorldBounds = true;
    this.fireball.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.fireball);
    this.fireball.body.velocity.x = -400;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 1239 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level3_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 2924 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level3_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 10000) {
    this.bgMusic.destroy();
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected2.alpha = 1;
    this.collected3.alpha = 0;

    this.dialogue1Said = false;
    this.dialogue2Said = false;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.isWallEnabled = false;
      this.createKey();
    }
  }
};

SIN.Level3Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level3Game.prototype.resetLevel = function() {
  this.fireballGroup.forEach(function(fire) {
    fire.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level3Game.prototype.createKey = function() {
  this.key = game.add.sprite(2695, 31, 'key3');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 51}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level3Game.prototype.playerHit = function() {
  //if hits on the right side, die
  console.log("Overlapping but no blocked.right");
  if(this.player.body.blocked.right) {
    //set to dead (this doesn't affect rendering)
    this.player.health -= 1;
    this.player.body.x -= 30;
    cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
    console.log("Health bar width at playerHit() - collision with barricadeLayer: ", cropWidth);
    this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
    if (this.player.health <= 0) {
        this.resetLevel();
    }
  }
};

SIN.Level3Game.prototype.playerWallHit = function() {
  //if hits on the right side, die
  if(this.down && this.wall.height > 10) {
      this.resetLevel();
  }
};

SIN.Level3Game.prototype.decrease = function() {
  if (this.wall.height > 0) {
    this.wall.crop(new Phaser.Rectangle(0, 0, this.wall.width, this.height));
    this.height -= 10;
  }
  if (this.height < 0) {
    this.down = false;
  }
};

SIN.Level3Game.prototype.increase = function() {
  if (this.wall.height < 490) {
    this.wall.crop(new Phaser.Rectangle(0, 0, this.wall.width, this.height));
    this.height += 10;
  }
  if (this.height > 490) {
    this.down = true;
    this.height = 490;
  }
};

SIN.Level3Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected2.alpha = 0;
    this.collected3.alpha = 1;
    this.isWallEnabled = true;
  }, this);
};

var SIN = SIN || {};

SIN.Level3Preload = function(){};

SIN.Level3Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level3Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading3');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level3', 'assets/tilemaps/level3.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Phase III Spritesheet', 'assets/images/level3/spritesheet.png');

  this.load.image('level3_bg_1', 'assets/images/level3/bg_1.png');
  this.load.image('level3_bg_2', 'assets/images/level3/bg_2.png');
  this.load.image('level3_bg_3', 'assets/images/level3/bg_3.png');
  this.load.image('level3_bg_4', 'assets/images/level3/bg_4.png');
  this.load.image('level3_bg_5', 'assets/images/level3/bg_5.png');

  this.load.image('axe1', 'assets/images/weapons/axe1.png');
  this.load.image('axe2', 'assets/images/weapons/axe2.png');
  this.load.image('axe3', 'assets/images/weapons/axe3.png');

  this.load.image('key3', 'assets/images/level3/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.audio('level3_dialogue1', 'assets/audio/vo_degir_face.ogg');
  this.load.audio('level3_dialogue2', 'assets/audio/vo_degir_key3.ogg');
  this.load.image('collected2', 'assets/images/collected2.png');
  this.load.image('collected3', 'assets/images/collected3.png');
  this.load.image('wall', 'assets/images/weapons/wall.png');
  this.load.image('fireball', 'assets/images/weapons/fireball.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level3Preload.prototype.create = function() {
  this.game.state.start('Level3Game');
};

var SIN = SIN || {};

SIN.Level4Game = function(){
  this.healthbar = [];
  this.player = [];
  this.timeCheck = [];
};

SIN.Level4Game.prototype = Object.create(Phaser.State.prototype);
SIN.Level4Game.prototype.constructor = SIN.Level2Game;

SIN.Level4Game.prototype.preload = function() {
  // For FPS
  this.game.time.advancedTiming = true;
};

SIN.Level4Game.prototype.create = function() {
  this.game.add.tileSprite(0, 0, 2170, 700, 'level4_bg_1');
  this.game.add.tileSprite(2170, 0, 2170, 700, 'level4_bg_2');
  this.game.add.tileSprite(4340, 0, 2170, 700, 'level4_bg_3');
  this.game.add.tileSprite(6510, 0, 2170, 700, 'level4_bg_4');
  this.game.add.tileSprite(8680, 0, 2170, 700, 'level4_bg_5');
  this.game.add.tileSprite(10850, 0, 2170, 700, 'level4_bg_6');

  this.map = this.game.add.tilemap('level4');

  //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
  this.map.addTilesetImage('Phase IV Spritesheet Foreground', 'Phase IV Spritesheet Foreground');

  //create layers
  this.backgroundlayer = this.map.createLayer('backgroundLayer');
  this.blockedLayer = this.map.createLayer('blockedLayer');
  this.pitLayer = this.map.createLayer('pitLayer');

  //collision on blockedLayer
  this.map.setCollisionBetween(1, 100000, true, 'blockedLayer');
  this.map.setCollisionBetween(1, 100000, true, 'pitLayer');

  //resizes the game world to match the layer dimensions
  this.backgroundlayer.resizeWorld();

  this.fireflies_1 = game.add.sprite(10717, 0, 'fireflies_1');
  this.game.add.tween(this.fireflies_1).to({x: 10757}, 1000, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
  this.fireflies_2 = game.add.sprite(10817, 0, 'fireflies_2');
  this.game.add.tween(this.fireflies_2).to({x: 10767}, 1000, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);

  this.spikeWheelGroup = game.add.physicsGroup();

  this.spike_1 = new SIN.SpikeWheel(game, 450, 1300, 460, 3000);
  this.spikeWheelGroup.add(this.spike_1);
  this.spike_3 = new SIN.SpikeWheel(game, 1920, 2420, 460, 2000);
  this.spikeWheelGroup.add(this.spike_3);
  this.spike_4 = new SIN.SpikeWheel(game, 4090, 4590, 460, 2000);
  this.spikeWheelGroup.add(this.spike_4);
  this.spike_5 = new SIN.SpikeWheel(game, 9796, 9927, 460, 800);
  this.spikeWheelGroup.add(this.spike_5);
  this.spike_6 = new SIN.SpikeWheel(game, 10258, 10127, 460, 800);
  this.spikeWheelGroup.add(this.spike_6);

  this.hammerGroup = game.add.physicsGroup();

  this.hammer1_1 = new SIN.Hammer1(game, 2940, -400, -25, 1250);
  this.hammerGroup.add(this.hammer1_1);
  this.hammer1_2 = new SIN.Hammer1(game, 8380, -400, 10, 1250);
  this.hammerGroup.add(this.hammer1_2);

  this.hammer2_1 = new SIN.Hammer2(game, 3675, -400, -60, 950);
  this.hammerGroup.add(this.hammer2_1);

  this.hammer3_1 = new SIN.Hammer3(game, 5664, -400, -60, 950);
  this.hammerGroup.add(this.hammer3_1);
  this.hammer3_2 = new SIN.Hammer3(game, 7130, -400, -60, 950);
  this.hammerGroup.add(this.hammer3_2);

  this.hammer4 = new SIN.Hammer4(game, 10990, -400, 10, 950);
  this.hammerGroup.add(this.hammer4);

  this.isKeyCollected = false;
  this.createKey();

  this.dialogue1Said = false;
  this.dialogue2Said = false;

  this.daggerGroup = game.add.physicsGroup();

  // creating custom sprite objects
  this.player = new SIN.Degir(game, 100, 160);
  //this.player = new SIN.Degir(game, 11400, 160);

  this.collected3 = this.game.add.sprite(20, 20, 'collected3');
  this.collected3.fixedToCamera = true;
  this.collected4 = this.game.add.sprite(20, 20, 'collected4');
  this.collected4.fixedToCamera = true;
  this.collected4.alpha = 0;

  this.healthbar = new SIN.HealthBar(game, 95, 30);
  this.timeCheck = game.time.now;

  //the camera will follow the player in the world
  this.game.camera.follow(this.player);

  this.bgMusic = this.game.add.audio('music_dark');
  this.bgMusic.loopFull(0.5);
};

SIN.Level4Game.prototype.update = function() {
  //collision
  this.game.physics.arcade.collide(this.player, this.blockedLayer);
  this.game.physics.arcade.collide(this.player, this.pitLayer, this.resetLevel, null, this);
  this.game.physics.arcade.collide(this.player, this.spikeWheelGroup, this.playerHit, null, this);
  this.game.physics.arcade.overlap(this.player, this.hammerGroup, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.knife, this.resetLevel, null, this);
  this.game.physics.arcade.overlap(this.player, this.key, this.keyHit, null, this);

  if (this.player.body.x > 2445 && this.player.body.x < 3500 && game.time.now - this.timeCheck > 4000) {
    this.knife = this.daggerGroup.create((this.player.body.x + 900), 360, 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -600;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x > 5750 && this.player.body.x < 7100 && game.time.now - this.timeCheck > 2000) {
    this.knife = this.daggerGroup.create((this.player.body.x + 900), 490, 'knife');
    this.knife.checkWorldBounds = true;
    this.knife.outOfBoundsKill = true;
    this.game.physics.arcade.enable(this.knife);
    this.knife.body.velocity.x = -600;
    this.timeCheck = game.time.now;
  }

  if (this.player.body.x >= 2620 && this.dialogue1Said === false) {
    this.voice1 = this.game.add.audio('level4_dialogue1');
    this.voice1.play();
    this.dialogue1Said = true;
  }

  if (this.player.body.x >= 9500 && this.dialogue2Said === false) {
    this.voice2 = this.game.add.audio('level4_dialogue2');
    this.voice2.play();
    this.dialogue2Said = true;
  }

  if (this.player.body.x >= 12359) {
    this.player.hanging = true;
    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    this.player.animations.play('turn', 2, false, false);
    this.player.animations.currentAnim.onComplete.add(function () {
      this.lastTween = this.game.add.tween(this.player).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
      this.lastTween.onComplete.add(function () {
        this.bgMusic.destroy();
        window.location.replace("epilogue.html");
        //this.game.state.start('EpiloguePreload');
      }, this);
    }, this);
  }

  if (this.player.deathAnimationComplete) {
    this.player.deathAnimationComplete = false;
    this.player.body.x = this.player.spawnPointX;
    this.player.body.y = this.player.spawnPointY;
    this.player.inputEnabled = true;
    this.healthbar.crop(new Phaser.Rectangle(0, 0, 160, 20));
    this.collected3.alpha = 1;
    this.collected4.alpha = 0;

    if(this.isKeyCollected) {
      this.isKeyCollected = false;
      this.createKey();
    }
  }
};

SIN.Level4Game.prototype.render = function() {
  //this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40px Courier");
  //this.game.debug.spriteInfo(this.player, 32, 32);
};

SIN.Level4Game.prototype.resetLevel = function() {
  this.daggerGroup.forEach(function(dag) {
    dag.kill();
  }, this);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, 0, 0));
  this.player.die();
};

SIN.Level4Game.prototype.createKey = function() {
  this.key = game.add.sprite(9998, 194, 'key4');
  this.game.physics.arcade.enable(this.key);
  this.game.add.tween(this.key).to({y: 224}, 700, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Level4Game.prototype.playerHit = function(player, spikeWheelGroup) {

  this.player.health -= 0.1;
  if (player.body.blocked.right) {
    this.player.body.x -= 30;
  } else if (player.body.blocked.left) {
    this.player.body.x += 30;
  }

  cropWidth = (this.player.health / this.player.maxHealth) * this.healthbar.width;
  console.log("Health bar width at playerHit() - collision with spikewheel: ", cropWidth);
  this.healthbar.crop(new Phaser.Rectangle(0, 0, cropWidth, this.healthbar.height));
  if (this.player.health <= 0) {
      this.resetLevel();
  }

};

SIN.Level4Game.prototype.keyHit = function() {
  this.isKeyCollected = true;
  var keyTween = this.game.add.tween(this.key).to( { alpha: 0 }, 300, Phaser.Easing.Linear.None).start();
  keyTween.onComplete.add(function () {
    this.key.kill();
    this.collected3.alpha = 0;
    this.collected4.alpha = 1;
  }, this);
};

var SIN = SIN || {};

SIN.Level4Preload = function(){};

SIN.Level4Preload.prototype = Object.create(Phaser.State.prototype);

SIN.Level4Preload.prototype.preload = function() {
  //show loading screen
  this.loading = this.game.add.sprite(0, 0, 'loading4');

  this.preloadBar = this.add.sprite((this.game.world.width - 75), (this.game.world.height - 25), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);

  //load game assets
  this.load.tilemap('level4', 'assets/tilemaps/level4.json', null, Phaser.Tilemap.TILED_JSON);
  this.load.image('Phase IV Spritesheet Foreground', 'assets/images/level4/spritesheet.png');

  this.load.image('level4_bg_1', 'assets/images/level4/bg_1.png');
  this.load.image('level4_bg_2', 'assets/images/level4/bg_2.png');
  this.load.image('level4_bg_3', 'assets/images/level4/bg_3.png');
  this.load.image('level4_bg_4', 'assets/images/level4/bg_4.png');
  this.load.image('level4_bg_5', 'assets/images/level4/bg_5.png');
  this.load.image('level4_bg_6', 'assets/images/level4/bg_6.png');
  this.load.image('fireflies_1', 'assets/images/level4/fireflies_1.png');
  this.load.image('fireflies_2', 'assets/images/level4/fireflies_2.png');

  this.load.image('key4', 'assets/images/level4/key.png');
  this.load.audio('music_dark', 'assets/audio/music_dark.ogg');
  this.load.audio('level4_dialogue1', 'assets/audio/vo_sin_laugh.ogg');
  this.load.audio('level4_dialogue2', 'assets/audio/vo_degir_key4.ogg');
  this.load.image('collected3', 'assets/images/collected3.png');
  this.load.image('collected4', 'assets/images/collected4.png');

  this.load.image('spike_wheel', 'assets/images/weapons/spike_wheel.png');
  this.load.image('hammer1', 'assets/images/weapons/hammer1.png');
  this.load.image('hammer2', 'assets/images/weapons/hammer2.png');
  this.load.image('hammer3', 'assets/images/weapons/hammer3.png');
  this.load.image('hammer4', 'assets/images/weapons/hammer4.png');
  this.load.image('knife', 'assets/images/weapons/knife.png');

  this.game.load.atlasJSONHash('sin_spritesheet', 'assets/images/degir/sin_spritesheet.png', 'assets/images/degir/sin_spritesheet.json');
};

SIN.Level4Preload.prototype.create = function() {
  this.game.state.start('Level4Game');
};

var SIN = SIN || {};

SIN.ProloguePreload = function(){};

SIN.ProloguePreload.prototype = Object.create(Phaser.State.prototype);

SIN.ProloguePreload.prototype.preload = function() {
  //show loading screen
  this.logo = this.game.add.sprite(this.game.world.centerX, (this.game.world.centerY - 20), 'logo');
  this.logo.anchor.setTo(0.5);

  this.preloadBar = this.add.sprite((this.game.world.centerX - 60), (this.game.world.centerY + 100), 'healthbar');
  this.preloadBar.anchor.setTo(0);
  this.preloadBar.scale.setTo(0.8);
  this.load.setPreloadSprite(this.preloadBar);
  this.load.image('prologue1', 'assets/images/prologue/1.png');
  this.load.image('prologue2', 'assets/images/prologue/2.png');
  this.load.image('prologue3', 'assets/images/prologue/3.png');
  this.load.image('prologue4', 'assets/images/prologue/4.png');
  this.load.image('prologue5', 'assets/images/prologue/5.png');
  this.load.image('esc', 'assets/images/prologue/esc.png');
  this.load.audio('prologue_dialogue', 'assets/audio/vo_sin_prologue.ogg');
  this.load.audio('prologue_degir_dialogue', 'assets/audio/vo_degir_prologue.ogg');
};

SIN.ProloguePreload.prototype.create = function() {
  this.game.state.start('Prologue');
};

var SIN = SIN || {};

SIN.Prologue = function(){};

SIN.Prologue.prototype = Object.create(Phaser.State.prototype);

SIN.Prologue.prototype.create = function() {
  this.playMusic();
  this.prologue1 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue1');
  this.prologue1.scale.setTo(0.5);
  this.prologue1.anchor.setTo(0.5);
  this.prologue1.alpha = 0;

  this.prologue2 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue2');
  this.prologue2.anchor.setTo(0.5);
  this.prologue2.alpha = 0;

  this.prologue3 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue3');
  this.prologue3.anchor.setTo(0.5);
  this.prologue3.alpha = 0;

  this.prologue4 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue4');
  this.prologue4.anchor.setTo(0.5);
  this.prologue4.alpha = 0;

  this.prologue5 = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'prologue5');
  this.prologue5.anchor.setTo(0.5);
  this.prologue5.alpha = 0;

  this.esc = this.game.add.sprite(this.game.world.width - 100, this.game.world.height - 50, 'esc');

  this.game.time.events.add(250, this.showInitial, this );

  this.game.time.events.add(25250, this.showScene3, this );

  this.game.time.events.add(44060, this.showScene4, this );

  this.game.time.events.add(48400, this.showLastScene, this );

  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.skipKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);

  // this.game.time.events.add(9250, this.showStory, this );
  // this.game.time.events.add(11500, this.translateCanvas, this );
  // this.game.time.events.add(70000, this.showInfo, this );
};

SIN.Prologue.prototype.update = function() {
  if (this.skipKey.isDown) {
    this.music.destroy();
    this.game.state.start('Level1Preload');
  }
};

SIN.Prologue.prototype.playMusic = function() {
  this.music = this.game.add.audio('prologue_dialogue');
  this.music.play();

  this.game.onPause.add(this.pauseMusic, this);
  this.game.onResume.add(this.resumeMusic, this);
};

SIN.Prologue.prototype.pauseMusic = function() {
  this.music.pause();
};

SIN.Prologue.prototype.resumeMusic = function() {
  this.music.resume();
};

SIN.Prologue.prototype.playDegir = function() {
  this.degir = this.game.add.audio('prologue_degir_dialogue');
  this.degir.play();

  this.degir.onPause.add(this.pauseDegir, this);
  this.degir.onResume.add(this.resumeDegir, this);
};

SIN.Prologue.prototype.pauseDegir = function() {
  this.degir.pause();
};

SIN.Prologue.prototype.resumeDegir = function() {
  this.degir.resume();
};

SIN.Prologue.prototype.showInitial = function() {
  this.prologue1Alpha = this.game.add.tween(this.prologue1).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  this.prologue1Alpha.onComplete.add(function () {
    this.prologue1Scale = this.game.add.tween(this.prologue1.scale).to({x: 0.7, y: 0.7}, 7000, Phaser.Easing.Linear.None).start();
    this.prologue1Scale.onComplete.add(function () {
      this.prologue1Alpha = this.game.add.tween(this.prologue1).to({alpha: 0}, 500, Phaser.Easing.Linear.None).start();
      this.prologue1Alpha.onComplete.add(function () {
        this.prologue2Alpha = this.game.add.tween(this.prologue2).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
      }, this);
    }, this);
  }, this);
};

SIN.Prologue.prototype.showScene3 = function() {
  this.prologue3Alpha = this.game.add.tween(this.prologue3).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
};

SIN.Prologue.prototype.showScene4 = function() {
  this.prologue2Alpha = this.game.add.tween(this.prologue2).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue3Alpha = this.game.add.tween(this.prologue3).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue3Alpha.onComplete.add(function () {
    this.prologue4Alpha = this.game.add.tween(this.prologue4).to({alpha: 1}, 200, Phaser.Easing.Linear.None).start();
  }, this);
};

SIN.Prologue.prototype.showLastScene = function() {
  this.prologue4Alpha = this.game.add.tween(this.prologue4).to({alpha: 0}, 200, Phaser.Easing.Linear.None).start();
  this.prologue5Alpha = this.game.add.tween(this.prologue5).to({alpha: 1}, 500, Phaser.Easing.Linear.None).start();
  this.playDegir();
  this.degir.onStop.addOnce(function() {
    this.prologue5Alpha = this.game.add.tween(this.prologue5).to({alpha: 0}, 1500, Phaser.Easing.Linear.None).start();
    this.prologue5Alpha.onComplete.add(function () {
      this.game.state.start('Level1Preload');
    }, this);
  }, this);
};

var SIN = SIN || {};

SIN.Acid = function (game, x, duration) {
  Phaser.Sprite.call(this, game, x, 700, 'acid');
  this.game.physics.arcade.enable(this);
  tween = game.add.tween(this).to({y: -100}, duration, Phaser.Easing.Elastic.None, true, 0, Infinity);
  this.minus = 1;
  tween.onComplete.add(function () {
    if (Phaser.Math.chanceRoll(50)) {
      this.minus = -1;
    }
    this.body.x = x + (this.minus * Math.random() * 20);
    this.body.y = 700;
  }, this);
};

SIN.Acid.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Acid.prototype.constructor = SIN.Acid;

var SIN = SIN || {};

SIN.Axe1 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe1');
  //this.body.immovable = true;
  //this.body.allowGravity = false;
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Axe1.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe1.prototype.constructor = SIN.Axe1;

var SIN = SIN || {};

SIN.Axe2 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe2');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Quartic.InOut, true, 0, Infinity, true);
};

SIN.Axe2.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe2.prototype.constructor = SIN.Axe2;

var SIN = SIN || {};

SIN.Axe3 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'axe3');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Axe3.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Axe3.prototype.constructor = SIN.Axe3;

var SIN = SIN || {};

SIN.Bat = function (game, startX, endX, y, duration) {
  Phaser.Sprite.call(this, game, startX, y, 'bat');
  this.game.physics.arcade.enable(this);
  this.animations.add('fly', Phaser.Animation.generateFrameNames('', 1, 8, '.png', 1), 10, true, false);
  this.animations.play('fly');
  if (startX > endX) {
    this.back = -1;
  } else {
    this.back = 1;
  }

  this.anchor.setTo(0.5);
  this.scale.setTo((1 * this.back), 1);
  toTween = game.add.tween(this).to({x: endX}, duration, Phaser.Easing.Quadratic.None).start();
  fromTween = game.add.tween(this).to({x: startX}, duration, Phaser.Easing.Quadratic.None);
  toTween.chain(fromTween);
  fromTween.chain(toTween);
  toTween.onComplete.add(function () {
    this.scale.setTo((-1 * this.back), 1);
    this.body.setSize(42, 137);
  }, this);
  fromTween.onComplete.add(function () {
    this.scale.setTo((1 * this.back), 1);
    this.body.setSize(105, 137);
  }, this);
};

SIN.Bat.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Bat.prototype.constructor = SIN.Bat;

SIN.Bat.prototype.update = function() {
  this.animations.play('fly');
};

var SIN = SIN || {};

SIN.Boat = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'Boat');
  this.game.add.existing(this);
  this.game.physics.arcade.enable(this);
  this.body.customSeparateX = true;
  this.body.customSeparateY = true;
  this.body.allowGravity = false;
  this.body.immovable = true;
  this.body.velocity.x = 0;
  this.anchor.x = 0.5;
  this.playerLocked = false;
};

SIN.Boat.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Boat.prototype.constructor = SIN.Boat;

var SIN = SIN || {};

SIN.Degir = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'sin_spritesheet');
  this.game.add.existing(this);

  this.inputEnabled = true;

  this.deathAnimationComplete = false;

  this.spawnPointX = x;
  this.spawnPointY = y;

  this.game.physics.arcade.enable(this);
  this.body.gravity.y = 1000;

  this.animations.add('stand', Phaser.Animation.generateFrameNames('', 31, 38, '.png', 2), 10, true, false);
  this.animations.add('run', Phaser.Animation.generateFrameNames('', 1, 8, '.png', 2), 10, true, false);
  this.animations.add('jump', Phaser.Animation.generateFrameNames('', 9, 16, '.png', 2), 10, true, false);
  this.animations.add('death', Phaser.Animation.generateFrameNames('', 17, 30, '.png', 2), 10, true, false);
  this.animations.add('hang', Phaser.Animation.generateFrameNames('', 39, 49, '.png', 2), 8, true, false);
  this.animations.add('turn', Phaser.Animation.generateFrameNames('', 50, 56, '.png', 2), 10, true, false);

  this.anchor.setTo(0.5, 1);

  this.health = 10.0;
  this.maxHealth = 10;

  this.hanging = false;
  this.showedRestartText = false;

  //move player with cursor keys
  this.cursors = this.game.input.keyboard.createCursorKeys();
  this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
  this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
  this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
};

SIN.Degir.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Degir.prototype.constructor = SIN.Degir;

SIN.Degir.prototype.update = function() {
  //only respond to keys and keep the speed if the player is alive
  if(this.inputEnabled) {
    if (!this.hanging) {
      if((this.cursors.right.isDown && this.cursors.up.isDown) || (this.rightKey.isDown && this.jumpKey.isDown)) {
        this.animations.play('jump');
        this.body.velocity.x = 300;
        this.scale.setTo(1);
      } else if((this.cursors.left.isDown && this.cursors.up.isDown) || (this.leftKey.isDown && this.jumpKey.isDown)) {
        this.animations.play('jump');
        this.body.velocity.x = -300;
        this.scale.setTo(-1, 1);
      } else if(this.cursors.right.isDown || this.rightKey.isDown) {
        if (this.body.blocked.down) {
          this.animations.play('run');
          this.scale.setTo(1);
        }
        this.body.velocity.x = 300;
      } else if(this.cursors.left.isDown || this.leftKey.isDown) {
        if (this.body.blocked.down) {
          this.animations.play('run');
          this.scale.setTo(-1, 1);
        }
        this.body.velocity.x = -300;
      } else if(this.cursors.up.isDown || this.jumpKey.isDown) {
        this.animations.play('jump');
        this.scale.setTo(1);
      } else if((!this.cursors.right.isDown || !this.cursors.left.isDown) || (!this.rightKey.isDown || !this.leftKey.isDown)) {
        this.body.velocity.x = 0;
        this.animations.play('stand');
        this.scale.setTo(1);
      }

      if(this.cursors.up.isDown || this.jumpKey.isDown) {
        this.playerJump();
      }

      if(this.x >= this.game.world.width) {
        if (this.game.state.current == 'Level1Game') {
          if(this.timer !== null) {
            game.time.events.remove(this.timer);
          }
          this.game.state.start('Level2Preload');
        } else if (this.game.state.current == 'Level2Game') {
          this.game.state.start('Level3Preload');
        } else if (this.game.state.current == 'Level3Game') {
          this.game.state.start('Level4Preload');
        } else {
          this.game.state.start('EpiloguePreload');
        }
      }
    }
  } else {
    this.die();
  }
};

SIN.Degir.prototype.playerJump = function() {
  if(this.body.blocked.down) {
    this.animations.play('jump');
    this.body.velocity.y -= 650;
  }
};

SIN.Degir.prototype.die = function() {
  this.inputEnabled = false;
  this.body.velocity.x = 0;
  this.body.velocity.y = 0;
  this.health = 10.0;
  this.animations.play('death', 10, false, false);
  this.animations.currentAnim.onComplete.add(function () {
    this.deathAnimationComplete = true;
  }, this);
};

var SIN = SIN || {};

SIN.Hammer1 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer1');
  //this.body.immovable = true;
  //this.body.allowGravity = false;
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Hammer1.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer1.prototype.constructor = SIN.Hammer1;

var SIN = SIN || {};

SIN.Hammer2 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer2');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Quartic.InOut, true, 0, Infinity, true);
};

SIN.Hammer2.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer2.prototype.constructor = SIN.Hammer2;

var SIN = SIN || {};

SIN.Hammer3 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer3');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Sinusoidal.InOut, true, 0, Infinity, true);
};

SIN.Hammer3.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer3.prototype.constructor = SIN.Hammer3;

var SIN = SIN || {};

SIN.Hammer4 = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'hammer4');
  this.game.add.existing(this);
  this.game.physics.arcade.enable(this);
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Elastic.InOut, true, 0, Infinity, true);
};

SIN.Hammer4.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Hammer4.prototype.constructor = SIN.Hammer4;

var SIN = SIN || {};

SIN.HealthBar = function (game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'healthbar');
  this.game.add.existing(this);
  //this.healthbar.scale.setTo(2);
  this.cropEnabled = true;
  this.fixedToCamera = true;
};

SIN.HealthBar.prototype = Object.create(Phaser.Sprite.prototype);
SIN.HealthBar.prototype.constructor = SIN.HealthBar;

var SIN = SIN || {};

SIN.Pellet = function (game, x, y, height, duration) {
  Phaser.Sprite.call(this, game, x, y, 'pellet');
  game.add.tween(this).to({y: height}, duration, Phaser.Easing.Circular.InOut, true, 0, Infinity, true);
};

SIN.Pellet.prototype = Object.create(Phaser.Sprite.prototype);
SIN.Pellet.prototype.constructor = SIN.Pellet;

var SIN = SIN || {};

SIN.SpikeWheel = function (game, startX, endX, y, duration) {
  Phaser.Sprite.call(this, game, startX, y, 'spike_wheel');
  this.game.physics.arcade.enable(this);
  this.body.allowGravity = false;
  this.body.immovable = true;
  if (startX > endX) {
    this.forward = false;
  } else {
    this.forward = true;
  }
  this.anchor.setTo(0.5);
  toTween = game.add.tween(this).to({x: endX}, duration, Phaser.Easing.Circular.None).start();
  fromTween = game.add.tween(this).to({x: startX}, duration, Phaser.Easing.Circular.None);
  toTween.chain(fromTween);
  fromTween.chain(toTween);
  toTween.onComplete.add(function () {
    this.forward = !this.forward;
  }, this);
  fromTween.onComplete.add(function () {
    this.forward = !this.forward;
  }, this);
};

SIN.SpikeWheel.prototype = Object.create(Phaser.Sprite.prototype);
SIN.SpikeWheel.prototype.constructor = SIN.SpikeWheel;

SIN.SpikeWheel.prototype.update = function() {
  if (this.forward) {
    this.angle += 2.5;
  } else {
    this.angle -= 2.5;
  }

};
